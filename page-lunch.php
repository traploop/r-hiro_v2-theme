<?php
/*
 Page : lunch
 
*/
global $siteInfo;

$siteInfo['xs__site_page_nav'] = false;


wp_enqueue_style( 'page-lunch', get_template_directory_uri() . '/css/page-lunch.css' );

get_header(); 
?>

<article <?php post_class('page-lunch'); ?>>
<?php
if( have_posts() ) : the_post();
?>
	<!--<figure class="bg_image"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>-->
	
	<div class="site_contents tempo_bg">
		<div class="row">
			<div class="col-xs-28 col-sm-14">
				<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
  					<ol class="carousel-indicators">
    					<li data-target="#carousel" data-slide-to="0" class="active"></li>
    					<li data-target="#carousel" data-slide-to="1"></li>
    					<li data-target="#carousel" data-slide-to="2"></li>
  					</ol>
  					<!-- Carousel items -->
  					<div class="carousel-inner">
  					  <div class="active item"><img src="/wp-content/themes/ristrante_hiro/images/<?php echo $siteInfo['blogSlug'] ?>/lunch_img1.jpg" class="img-responsive"></div>
  					  <div class="item"><img src="/wp-content/themes/ristrante_hiro/images/<?php echo $siteInfo['blogSlug'] ?>/lunch_img2.jpg" class="img-responsive"></div>
  					  <div class="item"><img src="/wp-content/themes/ristrante_hiro/images/<?php echo $siteInfo['blogSlug'] ?>/lunch_img3.jpg" class="img-responsive"></div>
  					</div>
  					<!-- Carousel nav -->
  					<!--<a class="carousel-control left" href="#carousel" data-slide="prev">&lsaquo;</a>
  					<a class="carousel-control right" href="#carousel" data-slide="next">&rsaquo;</a>-->
				</div>
			</div>

			<?php get_template_part( 'nav', 'site-page_nav-xs'); ?>
			
			<div class="col-xs-28 col-sm-14 contents-body">
				<div class="inner-wrapper lunch-text">
					<h2><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'.$name ?>_title.svg" class="content-titile"><span><?php
							echo esc_html(get_option("site_business_time_lunch")); ?></span></h2>
					<!-- START : WP contents -->

					<!-- END : WP contents -->
				
					<?php
						the_content();
			
						//get_template_part( 'content', 'shops_map');
					?>
				</div>
			</div>
		</div>
	</div>
		
<?php endif; ?>
</article>
<?php get_footer();
