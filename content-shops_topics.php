<?php
// Parts : Top : Shops Topics
global $siteInfo;
?>

<div class="content-shops_topics">
	<p class="title"><span class="text"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/common/txt-special_topics-shops.svg" alt="special topics" /></span></p>
<?php

$resTopics = new WP_Query(array(
	'category_name' => 'topics',
	'post_per_page' => 1,
	'order' => 'DESC',
	'orderby' => 'date',
));

if($resTopics->have_posts()) {
	$resTopics->the_post();
	$topicPost = $resTopics->posts[0];
	$imgOb = get_field('content_image', get_the_ID());
	$contactTel = stripslashes(esc_html(get_option('site_tel')));
	$contentLimitStr = get_field('content_limit_text', get_the_ID());
	$limitShopNameJp = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name_jp'];
	$limitShopNameEng = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name'];
?>
	<div class="topic-contents_area<?php echo (empty($imgOb) ? ' no_image' : ''); ?>">
<?php
	if(!empty($imgOb)) { ?>
		<figure><img src="<?php echo $imgOb['url']; ?>" alt="topic image"></figure>
<?php
	} ?>
		
		<div class="topics-info">
			<aside><span class="eng"><?php echo $limitShopNameEng; ?></span><span class="jpn"><?php echo $limitShopNameJp; ?>店</span></aside>
			
			<p class="title">『<?php the_title(); ?>』</p>
			
<?php
	if(!empty($contentLimitStr)) { ?>
			<p class="limit"><?php echo $contentLimitStr; ?></p>
<?php
	} ?>
			
			<dl class="contact-tel">
				<dt>ご予約・お問い合せ</dt>
				<dd><span class="hidden-xs"><?php echo $contactTel; ?></span><span class="visible-xs-inline"><a href="tel:<?php echo $contactTel; ?>"><?php echo $contactTel; ?></a></span></dd>
			</dl>
		</div>
		
		<div class="topics-contents"><?php the_content(); ?></div>
		
		<p class="link"><a href="<?php bloginfo('url'); ?>/topics/">More <i class="fa fa-angle-double-right"></i></a></p>
	</div>
<?php
}
?>

		
</div><!-- // .content-shops_topics -->
