<?php
global $siteInfo;

$siteTel = stripslashes(get_option('site_tel'));	// 店舗のTEL
$telno = str_replace("-", "", $siteTel);
$siteGoogleMapLinkSp = stripslashes(get_option('site_googlemap_link_sp'));	// 店舗のグーグルマップのへのリンク（スマホ用）

// global_page_nav
if ( has_nav_menu( 'globalMenu' ) ) : ?>
	<nav class="nav-site-global_menu site-nav visible-xs-block " role="navigation">
		<ul class="shop-access col-xs-28 row">
			<li class="col-xs-14"><span class="tel"><a href="tel:<?php echo $telno; ?>"><span class="glyphicon glyphicon-earphone"></span> <?php echo $siteTel; ?><i class="fa fa-angle-double-right"></i></a></span></li>
			<li class="col-xs-14"><a href="<?php echo $siteGoogleMapLinkSp; ?>"><span class="glyphicon glyphicon-map-marker"></span> Google Maps&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i></a></li>
		</ul>
		<?php
			// Global navigation menu.
			wp_nav_menu( array(
				'menu_class'     => 'nav-menu1 clearfix',
				'theme_location' => 'globalMenu',
				'link_before'    => '<span class="text">',
				'link_after'     => '</span><i class="fa fa-angle-double-right"></i>',
			) );
		?>
	</nav><!-- .main-navigation -->
<?php endif;

