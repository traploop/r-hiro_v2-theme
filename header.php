<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
global $siteInfo;

wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap_style.css', array(), '0.1');
wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '0.1', true );

$siteName = esc_html(get_option('site_name'));
$siteFb = stripslashes(esc_html(get_option('site_facebook')));
$bodyClassList = array(
	'site__'.$siteInfo['blogSlug'],
);
switch($siteInfo['blogSlug']) {
	case 'top': case 'overseas': $bodyClassList[] = 'site_style-'.$siteInfo['blogSlug']; break;
	default:  $bodyClassList[] = 'site_style-shops'; break;
}

// Build og
$ogImgAr = array(
	'url' => esc_url( get_template_directory_uri() ) . '/images/logo/'.$siteInfo['blogSlug'].'.png',
	'mime_type' => 'image/png',
	'width' => 356,
	'height' => 356,
);
if(is_home()) {
	$ogTitle = get_bloginfo('name');
	$ogUrl = get_bloginfo('url');
	$wImgAr = rhiro_getNewTopicsImage();
	if(!empty($wImgAr)) { $ogImgAr = $wImgAr; }
} else {
	$ogTitle = get_the_title();
	$ogUrl = get_the_permalink();
	
	switch($post->post_type) {
		case 'page':
			switch($post->post_name) {
				case 'topics':
					$wImgAr = rhiro_getNewTopicsImage();
					if(!empty($wImgAr)) { $ogImgAr = $wImgAr; }
					break;
			}
			break;
	}
}

$ogSiteName = get_bloginfo('name');


?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta description="青山・丸ビル・銀座・東京駅・大阪・博多のイタリア料理店 リストランテ・ヒロ 公式サイト。デート、記念日、接待、そしてちょっとしたお食事会などにご利用ください。個室、ウェディングやパーティのご案内も。">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script>(function(){document.documentElement.className='js'})();</script>
	<?php wp_head(); ?>
	<!-- START : OG ---------->
	<meta property="og:title" content="<?php echo $ogTitle; ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo $ogUrl; ?>" />
	<meta property="og:site_name" content="<?php echo $ogSiteName; ?>" />
<?php
if(!empty($ogImgAr)) { ?>		
	<meta property="og:image" content="<?php echo $ogImgAr['url']; ?>" />
	<meta property="og:image:type" content="<?php echo $ogImgAr['mime_type']; ?>" />
	<meta property="og:image:width" content="<?php echo $ogImgAr['width']; ?>" />
	<meta property="og:image:height" content="<?php echo $ogImgAr['height']; ?>" />
<?php 
}
?>
	<!-- END : OG ---------->
	<!--[if lte IE 10]>
	<link rel="stylesheet" id="ie-css" href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/ie.css?ver=0.1" type="text/css" media="all">
	<![endif]-->
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/respond.js"></script>
	<![endif]-->
</head>
<body <?php body_class(implode(' ', $bodyClassList)); ?>><?php

if(is_home()) { ?>
	<div id="page_loader" class="loader"><i class="icon  fa fa-spinner fa-spin"></i></div>
	<script>jQuery(window).on('load',function() {
		jQuery('#page_loader').fadeOut();
	});
	</script>
<?php
}	
	
?>
<header class="row"><div class="wrapper">
	<div class="pc-nav  hidden-xs">
<?php
$gnavClass = array();
switch($siteInfo['blogSlug']) {
	case 'top': case 'overseas':
		$gnavClass[] = 'col-sm-28';
		break;
	default: ?>
		<figure class="logo col-sm-3 col-sm-offset-1"><a href="<?php bloginfo('url'); ?>"><img class="svg" src="<?php echo get_template_directory_uri().'/images/logo/'.$siteInfo['blogSlug'].'.svg' ?>" alt="Logo : <?php echo $siteName; ?>"></a></figure>
<?php
		$gnavClass[] = 'col-sm-23';
		break;
}
?>        
		<nav id="global_nav" class="navbar navbar-default row <?php echo implode(' ', $gnavClass); ?>">

			<?php get_template_part( 'nav', 'shop_contact'); ?>
			<?php get_sidebar(); ?>
		</nav>
	</div><!--  // .pc-nav -->
	
	<div class="mobile-nav visible-xs row">
		<figure class="logo col-xs-8 col-xs-offset-1"><a href="<?php bloginfo('url'); ?>"><img class="svg img-responsive" src="<?php echo get_template_directory_uri().'/images/logo/'.$siteInfo['blogSlug'].'.svg' ?>" alt="Logo : <?php echo $siteName; ?>"></a></figure>
<?php
if(!empty($siteFb)) { ?>
		<div class="visible-xs shop-xs-top-fb"><p class="icon_facebook ">
			<a href="<?php echo $siteFb; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/images/common/icon-fb.svg" alt="facebook">
			</a>
		</p></div>	
<?php
		} 
?>	
<?php

$resCurrently = new WP_Query(array(
	'order' => 'DESC',
	'orderby' => 'date',
));

if($resCurrently) {
	$limitShopNameJp = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name_jp'];
	$limitShopNameEng = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name'];
?>
		<aside class="currently-shop"><span class="eng"><?php echo $limitShopNameEng; ?></span>&nbsp;&nbsp;<span class="jpn"><?php echo $limitShopNameJp; ?></span></aside>
<?php
	} ?>
<?php
		get_sidebar('xs');
?>
	</div><!-- // .mobile-nav -->
</div></header>

<div class="wrapper">