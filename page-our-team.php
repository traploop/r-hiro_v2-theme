<?php
global $siteInfo;
/*
 Page : our-team
 
*/
$bgImgOb = get_field('page_bg_image');

get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}

?>

<article <?php post_class('page-'.$name); ?>>
<?php if( have_posts() ) : the_post(); ?>
	<!--<figure class="bg_image"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>
	-->
	<div class="site_contents row tempo_bg">
		<div class="hidden-xs bg-image"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>our-team_bg.jpg" class=""></div>
		<div class="col-xs-28 row">
			<div class="col-xs-28 contents-body">
			<!-- START : WP contents -->
				<?php
					the_content();
				?>
				<!-- END : WP contents -->
			</div>
		</div>
	</div>
		
<?php endif; ?>
</article>
<?php get_footer();
