<?php
// Parts : Top : Topics
global $siteInfo;


$slideControlHtml = '';
$slideHtml = '';

?>
<?php
		$topicsPosts = array();
		$cnt = 0;
		foreach($siteInfo['blogInfoList'] as $blogSlug => $blogDat) {
			switch($blogSlug) {
				case 'top': break;
				default:
					// get post data
					switch_to_blog( $blogDat['data']['blog_id'] );
					
					$resPosts = new WP_Query(array(
						'posts_per_page' => 1,
						'category_name' => 'topics',
						'orderby' => 'date',
						'order' => 'DESC',
					));
					
					
					// View post data
					if($resPosts->have_posts()) {
						$resPosts->the_post();
						
						$shopNameEng = $siteInfo['blogInfoList'][$blogSlug]['name'];
						$shopNameJpn = $siteInfo['blogInfoList'][$blogSlug]['name_jp'];
						$imgOb = get_field('content_image', get_the_ID());
						$topicLimit = get_field('content_limit_text', get_the_ID());
						$topicFullText = get_the_content();
						$contactTel = stripslashes(esc_html(get_option('site_tel')));

						
						$wSlideHtml = '';
						$wSlideControlHtml = '';
						
						// View : Control list
						// $slideControlHtml .= '<li data-target="#home_slide" data-slide-to="'.$cnt.'"'. ($cnt == 0 ? ' class="active"' : '') .'></li>';
						
						
						// View : Main list
						$wSlideHtml .= '<figure><div class="img-vmiddle"><img class="img-responsive" src="'.$imgOb['url'].'" alt="topic : '.get_the_title().'"></div></figure>';
						$wSlideHtml .= '<div class="topics-info">';
						$wSlideHtml .= '<p class="shop_name"><span class="eng">'.$shopNameEng.'</span><span class="jpn">'.$shopNameJpn.'店</span></p>';
						$wSlideHtml .= '<aside class="shop_limit">'.$shopNameJpn.'店限定</aside>';
						$wSlideHtml .= '<p class="topic_title"><span class="text">『'.get_the_title().'』</span></p>';
						$wSlideHtml .= '<dl class="contact-tel"><dt>ご予約・お問い合せ</dt><dd><span class="hidden-xs">'.$contactTel.'</span><span class="visible-xs-inline"><a href="">'.$contactTel.'</a></span></dd></dl>';
						$wSlideHtml .= '<p class="topic_limit"><span class="text">'.$topicLimit.'</span></p>';
						$wSlideHtml .= '</div>';
						$wSlideHtml .= '<div class="topics-contents">'.$topicFullText.'</div>';
						
						$wSlideHtml = '<li class="topic_item shop-'.$blogSlug.'"><div class="click_area" onclick="rhiro_displayModal_blogPost(this); return false;" data-blog="'.$blogSlug.'" data-postid="'.get_the_ID().'">'.$wSlideHtml.'</div></li>';
						
						
						$slideHtml .= $wSlideHtml;
						wp_reset_query();		
					}
					$cnt++;
					
					break;
			}
		}
		switch_to_blog( $siteInfo['blogInfoList']['top']['data']['blog_id'] );
		
		//$slideControlHtml = '<ol class="carousel-indicators">'.$wSlideControlHtml.'</ol>';
		$slideHtml = '<ul id="slider_area">'.$slideHtml.'</ul>';


?>


<div <?php post_class('all_site_topics'); ?>>
	<p class="title"><span class="text"><img src="<?php echo get_template_directory_uri(); ?>/images/common/txt-special_topics-top.svg" alt="special topics" /></span></p>
	<div class="topic_list">
		<div class="loader"><i class="icon  fa fa-spinner fa-spin"></i></div>
		<div class="slider_view_area  area-hidden">
		<?php
		echo $slideControlHtml.$slideHtml;
		?>
		</div>
	</div>
	<script>
		var topicSlider = {
			'builder': false,
			'resp': false,
			'ob': false
		}
		jQuery(window).on('load', function(){
			rebuildSlider();
		});
		jQuery(window).resize(function() {
			if(topicSlider.builder === false) {
				topicSlider.builder = setTimeout(function() {
					var f_build = false;
					switch(topicSlider.resp) {
						case 'xxs': case 'xs':
							switch (responsibleInfo.checkRespWidthSize()) {
								case 'xxs': case 'xs': break;
								default: f_build = true; break;
							}
							break;
						default:
							switch (responsibleInfo.checkRespWidthSize()) {
								case 'xxs': case 'xs': f_build = true; break;
								default: break;
							}
							break;
					}
					if(f_build) {
						rebuildSlider();
					}
					topicSlider.builder = false;
				}, 300);
			}
		}); 
		
		function rebuildSlider() {
			jQuery('.topic_list .loader').show();
			if(topicSlider.ob !== false && topicSlider.ob !== undefined) {
				topicSlider.ob.destroySlider();
			}
			topicSlider.resp = responsibleInfo.checkRespWidthSize();
			topicSlider.ob = constractSlider(topicSlider.resp);
			jQuery('.slider_view_area').removeClass('area-hidden');
			jQuery('.topic_list .loader').hide();
		}
		
		function constractSlider(style) {
			var res;
			switch(style) {
				case 'xxs': case 'xs':
					res = jQuery('#slider_area').bxSlider({
						pager: false,
						minSlides: 1,
						maxSlides: 1,
						//slideWidth: 250,
						//slideMargin: 16,
						hideControlOnEnd: false,
						infiniteLoop: false,
						moveSlideQty: 1, //移動時にずれる数
						useCSS: true,
						autoStart: false
					});
					break;
				default:
					res = jQuery('#slider_area').bxSlider({
						pager: false,
						minSlides: 1,
						maxSlides: 4,
						slideWidth: 273,
						slideMargin: 16,
						hideControlOnEnd: false,
						infiniteLoop: false,
						moveSlideQty: 1, //移動時にずれる数
						useCSS: true,
						autoStart: false
					});
					break;
			}
			return res;
		}
	</script>


</div><!-- // topics_area -->
