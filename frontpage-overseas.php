<?php
// Parts : Front page : Top

global $siteInfo;

wp_enqueue_script('bxslider');
wp_enqueue_style('bxslider');




get_header(); ?>


<article <?php post_class(); ?>>
	<?php get_template_part( 'content', 'home_slide'); ?>
	
	<div class="inner-wrapper">
		<div class="welcome">
			<p class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/common/txt-welcome.svg" alt="Welcome" /></p>
			<div class="contents">
				<p>We take pride in being a pioneer of Japanese-style Italian cuisine and serve specialty dishes with joy and fun. It’s been for 20 years since we first opened RISTORANTE HiRo in Aoyama and now there are 4 restaurants in Tokyo and 1 restaurant in Osaka. 5 chefs in 5 different restaurants serve you their unique dishes in 5 characteristic locations. The fresh seasonal produce and the essence of Italian spirit make our dishes more delicious with nice surprise. The menu and the number of seats vary according to each restaurant and you can select a restaurant to suit your purpose and budget. We are looking forward to meeting you and hope that RISTORANTE HiRo will become one of your favorite restaurants.</p>
			</div>
		</div>
		
		
		<section class="store_detail aoyama clearfix">
			<div class="photo_name">
				<figure id="aoyama-carousel" class="photo carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#aoyama-carousel" data-slide-to="0" class="active"></li>
				    	<li data-target="#aoyama-carousel" data-slide-to="1" class=""></li>
				    </ol>
				    <div class="carousel-inner">
				    	<div class="item active">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/aoyama-photo1.jpg" alt="aoyama" />
				    	</div>
				    	<div class="item">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/aoyama-photo2.jpg" alt="aoyama" />
				    	</div>
				    </div>
  				</figure>
				<div class="name">
					<div class="detail_store_right">
						<ul>
							<li>
								<a href="http://www.opentable.jp/single.aspx?rid=18098&restref=18098" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/opentable-logo.png" alt="Open Table" class="opentable" />
								</a>
							</li>
							<li><a href="https://goo.gl/maps/XRcvnBskH6w" target="_blank">Google Maps  ></a></li>
							<li><a href="/aoyama/dinner/">Menu (Japanese) ></a></li>
						</ul>
					</div>
					<div class="detail_store_left">
						<span>RISTORANTE HiRo</span>
						<h3><img src="<?php echo get_template_directory_uri(); ?>/images/overseas/aoyama.svg" alt="aoyama" /></h3>
						<p class="tel">Telephone: 03-3486-5561</p>
					</div>
					<p class="text">We are located on the first basement level of “T-PLACE”. A two-minute walk from a subway exist B3 of Omotesando station. RISTRANTE HiRo was renewed in 2015. Inside of the restaurant has a relaxed ambience. You enjoy our dishes with wine in a comfortable space. We make use of our 20 years experience to cook well-selected produce as using our reliable technique with our productive sensibility. We are looking forward to serving you our tasty Japanese-style Italian cuisine. </p>
				</div><!--name-->
			</div><!--photo_name-->
			<div class="seats">
				<ul>
					<li>・The restaurant has 30 seats, which includes couple seats and sofas with partition arrangement and 4 to 6 face-to-face seats.・Lunch ¥3,000～ ・Dinner course ¥10,000～ ※Not including consumption tax </li>
				</ul>
			</div>
			<div class="store">
				<span>RISTORANTE HiRo AOYAMA</span><br>
				Address: T-PLACE B1F, 5-5-25 Minamiaoyama, Minato-ku, Tokyo / Telephone: 03-3486-5561 / 30 seats / Opening hours: [Lunch] 11:30 to 15:30 (Last order 14:00)<br>[Dinner] 18:00 to 23:00 (Last order 21:00) / Open every　day
			</div>
		</section><!--aoyama-->

		<section class="store_detail centro clearfix">
			<div class="photo_name">
				<figure id="centro-carousel" class="photo carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#centro-carousel" data-slide-to="0" class="active"></li>
				    	<li data-target="#centro-carousel" data-slide-to="1" class=""></li>
				    </ol>
				    <div class="carousel-inner">
				    	<div class="item active">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/centro-photo1.jpg" alt="centro" />
				    	</div>
				    	<div class="item">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/centro-photo2.jpg" alt="centro" />
				    	</div>
				    </div>
  				</figure>
				<div class="name">
					<div class="detail_store_right">
						<ul>
							<li>
								<a href="http://www.opentable.jp/single.aspx?rid=11993&restref=11993" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/opentable-logo.png" alt="Open Table" class="opentable" />
								</a>
							</li>
							<li><a href="https://goo.gl/maps/furUpoPEyxK2" target="_blank">Google Maps  ></a></li>
							<li><a href="/centro/dinner/">Menu (Japanese) ></a></li>
						</ul>
					</div>
					<div class="detail_store_left">
						<span>RISTORANTE HiRo</span>
						<h3><img src="<?php echo get_template_directory_uri(); ?>/images/overseas/centro.svg" alt="centro" /></h3>
						<p class="tel">Telephone: 03-5221-8331</p>
					</div>
					<p class="text">We are located on the 35th floor of Marunouchi Building in famous business district of a city just in front of Marunouchi side of JR Tokyo station. You will enjoy our dishes which match the sophisticated and historical Marunouchi in the open-plan floor 180 meters above the ground with a delightful outlook. We burn candles instead of lights at night so you enjoy the night view. There are 68 seats. A private room for 4 to 10 guests and a semi-private room for 16 to 20 guests. We hope you spend a wonderful time with lively cooking sound and delicious smell from the open kitchen.
 </p>
				</div><!--name-->
			</div><!--photo_name-->
			<div class="seats">
				<ul>
					<li>・Premium seats: ¥1,000 per person only for dinner time, which includes reserved window table charge ・Lunch ¥1,500～ ・Dinner course ¥10,000～  </li>
					<li>※Not including consumption tax ・All seats are no smoking・Please order vegetarian or vegan menu when you make a reservation. </li>
				</ul>
			</div>
			<div class="store">
				<span>RISTORANTE HiRo CENTRO Marunouchi Building</span><br>
				Address: Marunouchi Building 35F, 2-4-1 Marunouchi, Chiyoda-ku, Tokyo / Telephone: 03-5221-8331 / 68 seats, a private room for 4 to 10 guests<br>
				Opening hours: [Lunch] 11:00 to 15:30 (Last order 14:00) [Dinner] 18:00 to 23:00 (Last order 21:00) / Open every　day (Based on the opening hours of Marunouchi Building)<br>
				A two-minute walk from Marunouchi side of JR Tokyo station, one-minute walk from Tokyo station of Marunouchi line, a three-minute walk from a subway Otemachi station and a three-minute walk from Nijubashimae station of Chiyoda line.



			</div>
		</section><!--centro-->

		<section class="store_detail primo clearfix">
			<div class="photo_name">
				<figure id="primo-carousel" class="photo carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#primo-carousel" data-slide-to="0" class="active"></li>
				    	<li data-target="#primo-carousel" data-slide-to="1" class=""></li>
				    </ol>
				    <div class="carousel-inner">
				    	<div class="item active">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/primo-photo1.jpg" alt="primo" />
				    	</div>
				    	<div class="item">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/primo-photo2.jpg" alt="primo" />
				    	</div>
				    </div>
  				</figure>
				<div class="name">
					<div class="detail_store_right">
						<ul>
							<!--li>
								<a href="http://www.opentable.jp/single.aspx?rid=11993&restref=11993" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/opentable-logo.png" alt="Open Table" class="opentable" />
								</a>
							</li-->
							<li><a href="https://goo.gl/maps/ticnAXQFt4S2" target="_blank">Google Maps  ></a></li>
							<li><a href="/primo/dinner/">Menu (Japanese) ></a></li>
						</ul>
					</div>
					<div class="detail_store_left">
						<span>HiRo</span>
						<h3><img src="<?php echo get_template_directory_uri(); ?>/images/overseas/primo.svg" alt="primo" /></h3>
						<p class="tel">Telephone: 03-5219-5607</p>
					</div>
					<p class="text">We are located on the first floor in Kitchen Street area at JR Tokyo station. It is one-minute walk from Yaesu north exit of Tokyo station or a two-minute walk from Marunouchi north exit via a direct underground passage which is called Kitajiyutsuro. You enjoy the taste of RISTORANTE HiRo in a lighthearted and fun manner at our stylish restaurant. There are 32 seats and a very casual bar style of Italian.Please have a wonderful time at our restaurant in Kitchen Street area at JR Tokyo station just near the ticket gate of Shinkansen before departure. We offer the atmosphere of Little Italy within a thirty-second walking distance from the ticket gate and serve tasty pasta and delicate fusion of Italian with wine. 
</p>
				</div><!--name-->
			</div><!--photo_name-->
			<div class="seats">
				<ul>
					<li>・Pasta ¥1,200~, a small dish ¥700~, a glass of wine ¥600~ ※Not including consumption tax ・All seats are no smoking </li>
				</ul>
			</div>
			<div class="store">
				<span>HiRo PRIMO</span><br>
				Address: Kitchen Street 1F of Tokyo station, 1-9-1 Marunouchi, Chiyoda-ku, Tokyo / Telephone: 03-5219-5607 / 32 seats / Opening hours: [Lunch] 11:00 to 17:00  [Dinner] 18:00 to 23:00 (Last order 22:00) / Open every　day <br>
				Kitchen Street 1F of Tokyo station / One-minute walk from Yaesu North Exit of Tokyo station, a two-minute walk from Marunouchi North Exsit via a direct underground passage which is called Kitajiyutsuro
			</div>
		</section><!--primo-->

		<section class="store_detail ginza clearfix">
			<div class="photo_name">
				<figure id="ginza-carousel" class="photo carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#ginza-carousel" data-slide-to="0" class="active"></li>
				    	<li data-target="#ginza-carousel" data-slide-to="1" class=""></li>
				    </ol>
				    <div class="carousel-inner">
				    	<div class="item active">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/ginza-photo1.jpg" alt="ginza" />
				    	</div>
				    	<div class="item">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/ginza-photo2.jpg" alt="ginza" />
				    	</div>
				    </div>
  				</figure>
				<div class="name">
					<div class="detail_store_right">
						<ul>
							<li>
								<a href="http://www.opentable.jp/single.aspx?rid=18095&restref=18095" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/opentable-logo.png" alt="Open Table" class="opentable" />
								</a>
							</li>
							<li><a href="https://goo.gl/maps/yx1wgLFmr592" target="_blank">Google Maps  ></a></li>
							<li><a href="/ginza/dinner/">Menu (Japanese) ></a></li>
						</ul>
					</div>
					<div class="detail_store_left">
						<span>RISTORANTE HiRo</span>
						<h3><img src="<?php echo get_template_directory_uri(); ?>/images/overseas/ginza.svg" alt="ginza" /></h3>
						<p class="tel">Telephone: 03-3535-2750</p>
					</div>
					<p class="text">We are located on the seventh floor of Ginza Glasse. One-minute walk from a subway C8 exit of Ginza station, a two-minute walk from JR Yurakucho station and a five-minute walk from Hibiya station. The restaurant is full of grace yet casual and it suits a traditional and modern city. You enjoy the cheerful and peaceful atmosphere with the day light through a big window at lunch time and an elegant dinner with the night view in the evening. Inside of the restaurant is painted in quiet tones of color and there are 48 seats and each table for 2 to 4 guests. The restaurant also futures a counter with 6 seats and you can enjoy a view of the lively open kitchen even as per person. 

</p>
				</div><!--name-->
			</div><!--photo_name-->
			<div class="seats">
				<ul>
					<li>・Lunch ¥1,800～ ・Dinner course ￥4,500～ ※Not including consumption tax ・All seats are no smoking</li>
				</ul>
			</div>
			<div class="store">
				<span>RISTORANTE HiRo Ginza</span><br>
				Address: Ginza Glasse 7F, 3-2-15 Ginza, Chuo-ku, Tokyo / Telephone: 03-3535-2750 / 48 seats , 6 seats at a counter / Opening hours:[Lunch] 11:00 to 15:30 (Last order 14:00) [Dinner] 18:00 to 23:00 (Last order 21:00)Open every　day (Based on the opening hours of Ginza Glasse)<br>
				143 meters from Ginza station, C8 Exit is the closest
			</div>
		</section><!--ginza-->

		<section class="store_detail osaka clearfix">
			<div class="photo_name">
				<figure id="osaka-carousel" class="photo carousel slide carousel-fade" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#osaka-carousel" data-slide-to="0" class="active"></li>
				    	<li data-target="#osaka-carousel" data-slide-to="1" class=""></li>
				    </ol>
				    <div class="carousel-inner">
				    	<div class="item active">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/osaka-photo1.jpg" alt="osaka" />
				    	</div>
				    	<div class="item">
				    		<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/osaka-photo2.jpg" alt="osaka" />
				    	</div>
				    </div>
  				</figure>
				<div class="name">
					<div class="detail_store_right">
						<ul>
							<li>
								<a href="http://www.opentable.jp/single.aspx?rid=18101&restref=18101" target="_blank">
									<img src="<?php echo get_template_directory_uri(); ?>/images/overseas/opentable-logo.png" alt="Open Table" class="opentable" />
								</a>
							</li>
							<li><a href="https://goo.gl/maps/pXJ2tUrLJ1x" target="_blank">Google Maps  ></a></li>
							<li><a href="/osaka/dinner/">Menu (Japanese) ></a></li>
						</ul>
					</div>
					<div class="detail_store_left">
						<span>RISTORANTE HiRo</span>
						<h3><img src="<?php echo get_template_directory_uri(); ?>/images/overseas/osaka.svg" alt="osaka" /></h3>
						<p class="tel">Telephone: 06-6343-6966</p>
					</div>
					<p class="text">We are located on the fifth floor of “BREEZE BREEZE”. A short five-minute walk from JR Osaka station or Nishi-Umeda station via a direct underground passage. We take pride in being a pioneer of Japanese-style Italian cuisine. Our dishes are crafted with care from fresh seasonal produce from both the Kansai region and around the world. We hope you enjoy our bold yet delicate fusion of Italian and Japanese cuisine. Our spacious restaurant is made up of four different areas.
</p>
				</div><!--name-->
			</div><!--photo_name-->
			<div class="seats">
				<ul>
					<li>・The main dining area with a view of the lively open kitchen ・A private room for up to 16 guests, ・Semi-private rooms for 2 to 4 guests. ・The Pasteria area that’s available throughout our opening hours. This versatile space has a total of 56 seats, which includes 6 to 16 in the two private rooms and 18 in the Pasteria area.・Lunch ¥1,500～ ・Dinner course ¥5,000～ ※Not including consumption tax 
</li>
				</ul>
			</div>
			<div class="store">
				<span>RISTORANTE HiRo Osaka</span><br>
				Address: Breeze Breeze 5F, 2-4-9 Umeda, Kita-ku, Osaka  / Telephone: 06-6343-6966 / Opening hours: [Lunch] 11:30 to 15:30 (Last order 14:00) [Dinner] 18:00 to 23:00 (Last order 21:00) [Pasteria area] 11:00 to 23:00 (Last order 22:00)  / Open every　day (Based on the opening hours of BREEZE BREEZE)  / No smoking except the Pasteria area
			</div>
		</section><!--osaka-->
	</div><!--inner-wrapper-->
</article>
<script>constructDialog();</script>
<?php get_footer();
