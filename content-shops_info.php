<?php
// Parts : Top : Shops Topics
?>

<div <?php post_class('content-shops_info'); ?>>
	<p class="title"><span class="text"><img src="<?php echo get_template_directory_uri(); ?>/images/common/txt-information.svg" alt="information" /></span></p>
	<div class="topic_list"><?php
$resInfo = new WP_Query(array(
	'category_name' => 'information',
	'posts_per_page' => 3,
	'order' => 'DESC',
	'orderby' => 'date',
));

if($resInfo->have_posts()) {
?>

		<ul><?php 
	foreach($resInfo->posts as $resPost) {
?>
			<li><a href="#" onclick="rhiro_displayModal_post('<?php echo $resPost->ID; ?>'); return false;" title="「<?php echo $resPost->post_title; ?>」を読む"><p class="row"><span class="date  col-sm-4"><?php echo date_i18n('Y.m.d', strtotime($resPost->post_date)); ?></span><span class="title  col-sm-20"><?php echo $resPost->post_title; ?></span><span class="more  col-sm-4">More <i class="fa fa-angle-double-right"></i></span></p></a></li>
<?php
	}		
?>
		</ul>
<?php
}
?>
	</div><!-- // .topic_list -->
</div><!-- // .content-shops_info -->
