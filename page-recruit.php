<?php
/*
 Page : party
 
*/
global $siteInfo;

$siteInfo['xs__site_page_nav'] = false;



get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}


?>

<article <?php post_class('page-'.$name); ?>>
<div class="site_contents  row inner-wrapper">
		<header class="row">
			<h2 class="col-xs-28 col-sm-8"><img class="content-titile img-responsive" src="/wp-content/themes/ristrante_hiro/images/top/recruit.svg" alt="" /></h2>

			<hr class="col-xs-28 col-sm-offset-1 col-sm-19" />
			<p class="link_area  col-xs-28"><a href="/"><span class="text"><img src="/wp-content/themes/ristrante_hiro/images/shops/txt-group_home.svg" alt="GROUP HOME" /></span></a></p>
		</header>
<?php if( have_posts() ) : the_post(); ?>
	
	<div class="site_contents"><div class="inner-wrapper">
		<?php get_template_part('content', 'recruit'); ?>
	</div></div>
</div>
<?php endif; ?>
</article>
<?php get_footer();
