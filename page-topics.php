<?php
global $siteInfo;
/*
 Page : topics
 
*/


get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css', array(), '0.1.1' );
}


?>

<article <?php post_class('page-'.$name); ?>>
<?php
query_posts( array(
	'category_name' => 'topics',
	'posts_per_page' => $siteInfo['topics_num'],
	'order' => 'DESC',
	'orderby' => 'date',
));

if( have_posts() ) : ?>
	<div class="site_contents  tempo_bg">
		<div class="inner-wrapper topics-text">
			<h2 class="col-sm-28"><img src="<?php echo get_template_directory_uri().'/images/common/'; ?>txt-special_topics-shops-gray.svg" class="content-title" alt="special topics"></h2>
			<!-- START : WP contents -->
			<div class="topics_list col-sm-28"><ul>
<?php
	$shopTel = stripslashes(esc_html(get_option('site_tel')));
	$limitShopNameJp = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name_jp'];
	$limitShopNameEng = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name'];
	while( have_posts() ) : the_post();
		$imgOb = get_field('content_image', get_the_ID());
		if(!empty($imgOb)) { $imgUrl = $imgOb['url']; }
		else { $imgUrl = ''; }
		$limitStr = get_field('content_limit_text', get_the_ID());
		?>
						
				<li class="row"><?php
		if(!empty($imgUrl)) { ?>
					<figure class="col-sm-8"><img class="img-responsive" src="<?php echo $imgUrl; ?>" alt="「<?php the_title(); ?>」のイメージ写真"></figure>
<?php
		} ?>
					<div class="topics-contents-area  col-sm-19 col-sm-offset-1">
						<p class="shop_name  col-sm-28"><span class="eng"><?php echo $limitShopNameEng; ?></span><span class="jpn"><?php echo $limitShopNameJp; ?>店</span></p>
						<div class="topic-info  col-sm-9">
							<p class="post_title"><span class="text">『<?php the_title(); ?>』</span></p>
							<?php if(!empty($limitStr)) { ?><p class="limit_date"><span class="text"><?php echo $limitStr; ?></span></p><?php } ?>
							<p class="reservation"><dl><dt>ご予約・お問い合せ</dt><dd><span class="tel"><?php echo $shopTel; ?></span></dd></dl></p>
						</div>
						<div class="topics-contents  col-sm-18 col-sm-offset-1">
	<?php
			the_content();
	?>
						</div>
					</div>
					
				</li>
<?php
	endwhile;
?>
			</ul></div>
			<!-- END : WP contents -->
		</div>
	</div><!-- // .site_contents -->
<?php
endif;
wp_reset_query();
?>
</article>
<?php get_footer();
