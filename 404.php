<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">404 : ページが見つかりませんでした。</h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p>ページのアドレスを確認して下さい。</p>
					<p class="back2home"><a href="<?php bloginfo('url'); ?>">サイトトップへ戻る <i class="fa fa-angle-double-right"></i></a></p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
