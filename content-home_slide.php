<?php

global $siteInfo;


$f_side = false;
$slideImgDir = '/images/'.$siteInfo['blogSlug'].'/home_slide/';
$pgList = array();
if(
		file_exists(get_template_directory() . $slideImgDir) &&
		$dp = opendir(get_template_directory() . $slideImgDir)
) {
	$pgList = array();
	while (false !== ($fn = readdir($dp))) {
		if(strcmp(".",$fn) != 0 && strcmp("..",$fn) != 0) {
			$ar = array();
			if(preg_match('/(\d+)(\-(xs|lg))\.(jpg|png)/i', $fn, $ar)) {
				if(!isset($pgList[$ar[1]])) { $pgList[$ar[1]] = array(); }
				$pgList[$ar[1]][$ar[3]] = $fn;
			}
		}
	}
	closedir($dp);
}


if(!empty($pgList)) {
	sort($pgList);
	$slideControlHtml = '';
	$slideHtml = '';

	$cnt = 0;
	$max = count($pgList);
	foreach($pgList as $imgAr) {
		$wSlideHtml = '';
		$slideControlHtml .= '<li data-target="#home_slide" data-slide-to="'.$cnt.'"'. ($cnt == 0 ? ' class="active"' : '') .'></li>';
		if(isset($imgAr['lg'])) {
			$wSlideHtml .= '<img class="hidden-xxs hidden-xs img-responsive" src="'. get_template_directory_uri().$slideImgDir.$imgAr['lg'] . '" alt="Slide image '.$imgAr['lg'].'" />';
		}
		if(isset($imgAr['xs'])) {
			$wSlideHtml .= '<img class="hidden-sm hidden-md hidden-lg img-responsive" src="'. get_template_directory_uri().$slideImgDir.$imgAr['xs'] . '" alt="Slide image '.$imgAr['xs'].'" />';
		}
		$slideHtml .= '<li class="item'. ($cnt == 0 ? ' active' : '') .'">'.$wSlideHtml.'</li>';
		$cnt++;
	}
	$slideControlHtml = '<ol class="carousel-indicators">'.$slideControlHtml.'</ol>';
	$slideHtml = '<figure id="home_slide" class="carousel slide carousel-fade  row" data-ride="carousel"><div class="loader"><div class="img-vmiddle"><span class="icon  glyphicon glyphicon-refresh glyphicon-refresh-animate"></span></div></div>'.$slideControlHtml.'<ul class="carousel-inner">'.$slideHtml.'</ul></figure>';
	echo $slideHtml;
} else { ?>
		<figure><img class="img-responsive" src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>
<?php
}
?>
<script>
	jQuery('.carousel img:last').get(0).onload = function() {
		jQuery('#home_slide .loader').hide();
	};
</script>
	
