<?php
// Parts : Front page : Shops

global $siteInfo;

$siteInfo['xs__site_page_nav'] = false;

$siteOt = stripslashes(esc_html(get_option('site_opentable')));
$limitShopNameJp = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name_jp'];
$limitShopNameEng = $siteInfo['blogInfoList'][$siteInfo['blogSlug']]['name'];


get_header(); ?>


<article <?php post_class(); ?>>
	<!-- START : 'content', 'home_slide' -->
	<?php get_template_part( 'content', 'home_slide'); ?>
	<!-- END : 'content', 'home_slide' -->
	
	<!-- START : 'nav', 'site-page_nav-xs' -->
	<?php get_template_part( 'nav', 'site-page_nav-xs'); ?>
	<!-- END : 'nav', 'site-page_nav-xs' -->
	
	<div class="home_main_contents">
		<div class="site_contents">
			<!-- START : 'content', 'shops_topics' -->
			<?php get_template_part( 'content', 'shops_topics'); ?>
			<!-- END : 'content', 'shops_topics' -->
			
			
			<!-- START : 'content', 'shops_info' -->
			<?php get_template_part( 'content', 'shops_info'); ?>
			<!-- END : 'content', 'shops_info' -->
		</div><!-- // .site_contents -->
		
		<div class="home_news hidden-xs">
			<?php get_template_part( 'content', 'facebook'); ?>
		</div><!-- // .home_news -->
<?php
if(!empty($siteOt)) { ?>
		<div class="visible-xs shop-xs-top-Ot"><p class="icon_opentale">
			<a href="<?php echo $siteOt; ?>" target="_blank">
				<img src="<?php echo get_template_directory_uri(); ?>/images/common/icon-opentable.png" alt="open table" class="img-responsive">
			</a>
		</p></div>	
<?php
} 
?>	
	</div><!-- // .home_main_contents -->
</article>
<script>constructDialog();</script>

<?php get_footer();
