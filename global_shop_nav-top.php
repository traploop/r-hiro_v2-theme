<?php
// Parts : Global shop nav : Top

global $blog_id, $siteInfo;

?>

<div class="global_shop_nav-top row">
	<ul class="col-xs-offset-1 col-xs-26"><?php
$f_printLogo = false;
$cnt = 0;
$listLen = count($siteInfo['blogInfoList']);
foreach($siteInfo['blogInfoList'] as $wBlogKey => $wBlog) {
	switch($wBlogKey) {
		case 'top': break;
		default:
			if(!$f_printLogo && ($cnt > ($listLen * 0.5))) { ?>
		<li class="logo col-sm-4"><a href="/">
			<figure><img src="<?php echo get_template_directory_uri() . '/images/logo/top.svg'; ?>" alt="リストランテ・ヒロ" class="img-responsive"></figure>
		</a></li>
<?php
				$f_printLogo = true;
			}
			$class = array($wBlogKey);
?>
		<li class="<?php echo implode(' ', $class); ?> col-sm-4"><a href="<?php echo $wBlog['url']; ?>">
			<p class="name"><span class="text"><?php echo $wBlog['name']; ?></span></p>
			<p class="subname"><span class="text"><?php echo $wBlog['name_jp']; ?></span></p>
		</a></li>
<?php
			break;
	}
	$cnt++;
}
?>
	</ul>
</div><!-- // .global_shop_nav-top -->
