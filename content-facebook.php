<?php
// Parts : Facebook SNS Plugin

global $siteInfo;

$fbUrl = stripslashes(esc_html(get_option('site_facebook')));
if(empty($fbUrl)) { $fbUrl = $siteInfo['facebook_url']; }
?>

<div <?php post_class('content-facebook'); ?>>
	<p class="title"><span class="text"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/common/txt-news.svg" alt="NEWS" /></span></p>
	<div class="facebook_plugin">
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		 var js, fjs = d.getElementsByTagName(s)[0];
		 if (d.getElementById(id)) return;
		 js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=911351265581906&version=v2.0";
		 fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
		
		
		<div class="fb-like-box" data-href="<?php echo $fbUrl; ?>" data-width="516" data-hide-cover="true" data-height="573" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
	</div>
</div><!-- // topics_area -->
