<?php
global $siteInfo;
/*
 Page : access
 
*/
$bgImgOb = get_field('page_bg_image');


$siteName = stripslashes(get_option('site_name'));										// 店舗の名称
$siteAddr = stripslashes(get_option('site_address'));										// 店舗の住所
$siteTel = stripslashes(get_option('site_tel'));
$telno = str_replace("-", "", $siteTel);											// 店舗のTEL
$siteSeat = stripslashes(get_option('site_seat'));											// 店舗の座席
$siteBusTime = stripslashes(get_option('site_business_time'));							// 店舗の営業時間
$siteLunchTime = stripslashes(get_option('site_business_time_lunch'));					// 店舗の営業時間:ランチ
$siteDinnerTime = stripslashes(get_option('site_business_time_dinner'));					// 店舗の営業時間:ディナー
$siteOtherTime = stripslashes(get_option('site_business_time_other'));					// 店舗の営業時間:その他
$siteHoli = stripslashes(get_option('site_holiday'));										// 店舗の定休日
$siteGoogleMap = stripslashes(get_option('site_googlemap_iframe'));			// 店舗のグーグルマップの埋め込み用 iframe
$siteGoogleMapLinkSp = stripslashes(get_option('site_googlemap_link_sp'));	// 店舗のグーグルマップのへのリンク（スマホ用）
$siteStation = stripslashes(get_option('site_closest_station'));
get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}

?>

<article <?php post_class('page-'.$name); ?>>
<?php if( have_posts() ) : the_post(); ?>
	<!--<figure class="bg_image"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>
	-->
	<div class="site_contents row  tempo_bg">
		<div class="hidden-xs bg-image"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>access_bg.jpg" class=""></div>
		<div class="col-xs-28 row">
			<div class="col-xs-28 contents-body">
				<div class="inner-wrapper access-text col-xs-28 col-sm-14 col-sm-offset-2">
					<h2 class="visible-xs"><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'.$name ?>_title-xs.svg" class="content-titile"></h2>
					<h2 class="hidden-xs col-sm-28"><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'.$name ?>_title.svg" class="content-titile"></h2>
					<!-- START : WP contents -->

					<h3><?php if(!empty($siteName)) { ?><span class="name"><?php echo $siteName; ?></span><?php } ?></h3>
					<div class="shop_contact_info_area1"><?php 
						if(!empty($siteAddr)) { ?><p class="address"><span class="address-item">住所</span>　<?php echo $siteAddr; ?></p><?php }
						if(!empty($siteGoogleMapLinkSp)) { ?><p class="sp-map visible-xs"><a href="<?php echo $siteGoogleMapLinkSp; ?>">Google Maps&nbsp;&nbsp;<i class="fa fa-angle-double-right"></i></a></p><?php }
						if(!empty($siteTel)) { ?><p class="tel"><span class="address-item">TEL</span>　<a href="tel:<?php echo $telno; ?>"><?php echo $siteTel; ?></a></p><?php }
						if(!empty($siteSeat)) { ?><p class="seat"><span class="address-item">席数</span>　<?php echo $siteSeat; ?></p><?php }
					 ?></div>
					<div class="open_time row">
						<div class="col-xs-28 col-sm-3">営業時間</div>
						<div class="col-xs-28 col-sm-25"><?php
							if(!empty($siteLunchTime)) { ?><p class="business_time">&nbsp;&nbsp;ランチ　<?php echo nl2br($siteLunchTime); ?></p><?php }
							if(!empty($siteDinnerTime)) { ?><p class="business_time">&nbsp;&nbsp;ディナー　<?php echo nl2br($siteDinnerTime); ?></p><?php }
							if(!empty($siteOtherTime)) { ?><p class="business_time business_time_other"><?php echo nl2br($siteOtherTime); ?></p><?php } 
						?></div>
						<?php if(!empty($siteHoli)) { ?><p class="holiday">定休日　<?php echo nl2br($siteHoli); ?></p><?php } ?>
					</div>
					<div class="shop_contact_info_area2">
						<div class="col-xs-28 visible-xs">最寄駅</div>
						<?php if(!empty($siteGoogleMap)) { ?><div class="map  hidden-xs"><?php echo $siteGoogleMap; ?></div><?php } ?>
						<?php if(!empty($siteStation)) { ?><p class="closest_station"><?php echo nl2br($siteStation); ?></p><?php } ?>
					</div>
					<p class="disclaimer"><a href="mailto:info@r-hiro.com">※ご質問、コメントなど皆様のお声はこちらまで</a>（ご予約はお電話でのみ承っております）</p>
					<!-- END : WP contents -->
					<?php
						the_content();
					?>
				</div>
			</div>
		</div>
	</div>
		
<?php endif; ?>
</article>
<?php get_footer();
