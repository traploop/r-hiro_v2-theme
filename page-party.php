<?php
/*
 Page : party
 
*/
global $siteInfo;

$siteInfo['xs__site_page_nav'] = false;



get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}


?>

<article <?php post_class('page-'.$name); ?>>
<?php if( have_posts() ) : the_post(); ?>
	<!--<figure class="bg_image"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>-->
	
	<div class="site_contents tempo_bg">
		<div class="row">
			<div class="col-xs-28 col-sm-14"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'.$name; ?>_img1.jpg" class="img-responsive"></div>

			<?php get_template_part( 'nav', 'site-page_nav-xs'); ?>
			
			<div class="col-xs-28 col-sm-14 contents-body">
				<div class="inner-wrapper party-text">
					<h2><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'.$name ?>_title.svg" class="content-titile"></h2>
					<!-- START : WP contents -->
					
					<!-- END : WP contents -->
					<?php
						the_content();
					?>
				</div>
			</div>
		</div>
	</div>
		
<?php endif; ?>
</article>
<?php get_footer();
