/***************************************************************************************************
 システムで用意する Javascript の関数群
 ver 0.2.1
 Create : Shigeo Tsukamoto
 Modification Date : 2014/07/01
***************************************************************************************************/
//////////////////////////////////////////////////////////////
// Javascript Extension
//////////////////////////////////////////////////////////////
// Trim
String.prototype.trim = function() {
 return this.replace(/^[\s　]+|[\s　]+$/g, '');
};

//////////////////////////////////////////////////////////////
// Functions
//////////////////////////////////////////////////////////////

function createToTopButton() {
	// To top button
	var toTopMenuPos = 'body > #wrapper > footer';
	if(jQuery(toTopMenuPos).length > 0) {
		var toTopHtml = '';
		toTopHtml += '<nav id="to_top_menu_area">';
		toTopHtml += '<div id="to_top_menu"><a class="scroll_to_anchor" href="#"><span class="button"><img src="/images/button_totop.png" alt="To Page top" /></span><span class="button_shadow"></span></a></div>';
		toTopHtml += '</nav>';
		jQuery(toTopMenuPos).before(toTopHtml);
	}
}

function cnvTelLink() {
	// tellink のクラスに tel: のアンカーを作る。
	// 番号はクラス内の telnum クラスのテキストか、tellink クラスのテキストを使用する。
	var cl = '.tellink';
	var tlNumCl = '.telnum';
	if(jQuery(cl).length > 0) {
		var clAr = jQuery(cl);
		for(var cnt = 0; cnt < jQuery(cl).length; cnt++) {
			var anc = '';
			if(jQuery(clAr[cnt]).children(tlNumCl).length > 0) {
				anc = jQuery(clAr[cnt]).children(tlNumCl).text();
			} else {
				anc = jQuery(clAr[cnt]).text();
			}
			jQuery(clAr[cnt]).html('<a href="tel:'+anc+'">'+jQuery(clAr[cnt]).html()+"</a>");
		}
	}
	return true;
}



// エリア内の画像をエリアに合うように拡大縮小する
function areaFullsizeImg(el, callbackFunc) {
	for(var i=0; i < el.length; i++) {
		var areaOb = el[i];
		var cOb = jQuery(areaOb).find('img:first');
		if(cOb.length > 0) {
			var o = cOb.get(0);
			
			var areaW = jQuery(areaOb).width();
			var areaHeight = jQuery(areaOb).height();
			var minWidth = areaW;
			
			if(minWidth !== null && minWidth > areaW) { areaW = minWidth; }
			
			jQuery(o).css({'height': 'auto', 'width': 'auto', 'position': 'relative'});
			var wWidth = o.width;
			var wHeight = o.height;
			var heightRate = wHeight / areaHeight;
			var widthRate = wWidth / areaW;
			
			if(heightRate > 1) {
				jQuery(o).height(areaHeight);
				wHeight = areaHeight;
				wWidth /= heightRate;
				widthRate = wWidth / areaW;
				heightRate = 1;
			}
			if(wHeight < areaHeight) {
				wHeight = areaHeight;
				wWidth /= heightRate;
				widthRate = wWidth / areaW;
				jQuery(o).height(areaHeight);
				jQuery(o).css('width', 'auto');
				heightRate = 1;
			}
			if(wWidth < areaW) {
				wWidth = areaW;
				wHeight /= widthRate;
				heightRate = wWidth / areaHeight;
				widthRate = 1;
				jQuery(o).width(areaW);
				jQuery(o).css('height', 'auto');
			}
			var topPos = (areaHeight-wHeight)/2;
			var leftPos = (areaW-wWidth)/2;
			jQuery(o).css({'top': topPos+'px', 'left': leftPos+'px'});
		}
	}
	if(callbackFunc !== undefined) { callbackFunc(); }
}
// エリア内の .full_bg 内の画像をエリアに合うように拡大縮小する
function areaFullsizeBgImg(o, callbackFunc) {
	areaFullsizeImg(jQuery(o).find('.full_bg'), callbackFunc);
}
jQuery(window).resize(function(){
	areaFullsizeImg(jQuery('.full_bg'));
});




// Serialize
// PHP からの値をシリアライズするための関数
// 通常の serialize だけではうまく復元できないため、この関数を使う
function rhiro_serializeValiable(val) {	// new ver 0.1.1.1
	val = val.replace(/"/g, '__=EP_DQT_EP=__');
	val = val.replace(/'/g, '__=EP_SQT_EP=__');
	val = val.replace(/\n/g, '__=EP_RTN_EP=__');
	val = val.replace(/\r/g, '__=EP_CRTN_EP=__');
	val = val.replace(/\\/g, '__=EP_BKSL_EP=__');
	return val;
}
function rhiro_unserializeValiable(val) {	// new ver 0.1.1.1
	val = val.replace(/__=EP_DQT_EP=__/g, '"');
	val = val.replace(/__=EP_SQT_EP=__/g, "'");
	val = val.replace(/__=EP_RTN_EP=__/g, "\n");
	val = val.replace(/__=EP_CRTN_EP=__/g, "\r");
	val = val.replace(/__=EP_BKSL_EP=__/g, "\\");
	return val;
}



// Dialogs WP - bootstrap

// Dialog を使用するための準備
function constructDialog() {
	jQuery(window).on('load', function() {
		jQuery("body").children().last().after('<div id="dialog_area" style=""><div id="od_open_dialog"></div></div>');
	});
}
// ダイアログを開くための準備
function buildOpenDialog(id) {
	jQuery('#'+id).modal({
		//  背景をオーバーレイするか否か。(デフォルト) true
		//  'static'を指定した場合は、背景をクリックしてもモーダルを閉じない。
		//backdrop: 'static',
		//  エスケープキーで閉じるか否か。(デフォルト) true
		//keyboard: false,
		//  訪問時に自動的に表示を行うか否か。(デフォルト) true
		show: false
	});
}
// ダイアログを作成する
function createDialog(htmlStr, id) {
	if(id !== undefined) {
		if(jQuery("#"+id).length > 0) {
			jQuery("#"+id).remove();
		}
			jQuery("#od_open_dialog").after(htmlStr);
		buildOpenDialog(id);
	} else {
		jQuery("#od_open_dialog").after(htmlStr);
	}
}
// ダイアログを削除する
function removeDialog(id) {
	jQuery("#"+id).remove();
	jQuery("#od_"+id).remove();
}
// ダイアログを開く
function openDialog(id) {	// bootstrap
	if(jQuery("#"+id).length != 0) {
		/*
		if(jQuery("#od_"+id).length == 0) {
			buildOpenDialog(id);
		}
		*/
		if(jQuery("#od_"+id).length == 0) {
			//jQuery("#od_open_dialog").after('<a id="od_'+id+'" rel="dialog_control" data-controls-modal="'+id+'" data-backdrop="true" class="btn primary" href="#'+id+'">OPEN</a>');
			buildOpenDialog('od_'+id);
		}
		//jQuery("#od_"+id).click();
		jQuery('#'+id).modal('show');
	}
}
// ダイアログを閉じる
function closeDialog() {
	jQuery('#topics-centro-post_72').modal('hide');
	//$.colorbox.close();
}




function getHtml_modal_topics(dat) {
	if(dat === undefined) { return false; }
	
	var html = '';
	html += '<div id="'+dat.id+'" class="modal-topics  modal fade" role="dialog" aria-labelledby="'+dat.id+'" aria-hidden="true" style="display:none;"><div class="modal-dialog">';
	html += 	'<div class="modal-content">';
	html += 		'<div class="modal-body">'+dat.html+'</div>';
	html += 		'<div class="modal-footer">';
	html += 			'<button type="button" class="btn btn-default btn-primary" data-dismiss="modal">閉じる</button>';
	html += 		'</div>';
	html += 	'</div>';
	html += '</div></div>';

	
	return html;
}


function rhiro_displayModal_blogPost(dat) {
	var opid = 'topics-'+jQuery(dat).data('blog')+'-post_'+jQuery(dat).data('postid');
	var html = getHtml_modal_topics({ 'id':opid, 'html': jQuery(dat).html()});
	
	createDialog(html, opid);
	openDialog(opid);
	
	return false;
}




// ! Ajax ========================================================
function rhiro_displayModal_post(id) {
	
	 var mysack = new sack( get_bloginfo( 'wpurl' ) + "/wp-admin/admin-ajax.php" );
	
	 mysack.execute = 1;
	 mysack.method = 'POST';
	 mysack.setVar( "action", "rhiro_ajax_displayModal_post" );
	
	 mysack.setVar( "postId", id );
	 mysack.onError = function() { alert('Ajax error in looking up "rhiro_ajax_getRecruitPost_top".' )};
	
	 mysack.runAJAX();
	
	 return false;
}






//////////////////////////////////////////////////////////////
// Utility
//////////////////////////////////////////////////////////////
// レスポンシブの情報を管理する
var responsibleInfo = {
	lg: 1280,
	md: 1280,
	sm: 1280,
	xs: 414,
	xxs: 320,
	sizeClass: false,
	construct: function() {
		jQuery(window).resize(function() {
			responsibleInfo.setRespClass();
		})
		responsibleInfo.setRespClass();
	},
	setRespClass: function () {
		jQuery('html').removeClass(this.sizeClass);
		this.sizeClass = 'resp-'+this.getSize();
		jQuery('html').addClass(this.sizeClass);
	},
	getSize: function () {
		var w = jQuery(window).width();
		if(this.xxs >= w) { return 'xxs'; }
		else if(this.xs >= w) { return 'xs'; }
		else if(this.sm >= w) { return 'sm'; }
		else if(this.md >= w) { return 'md'; }
		else { return 'lg'; }
	},
	checkRespWidthSize: function() {
		return this.getSize();
	}
}


// ブラウザーに関する情報
var brwsInfo = {
	f_win: false,
	f_ie: false,
	f_ff: false,
	f_chrome: false,
	f_ipad: false,
	f_iphone:false,
	f_ios: false,
	f_android: false,
	f_mobile: false,
	f_tablet: false,
	brws_ver: false,
	os_ver: false,
	//f_cookie: checkCookie(),
	get: null,
	getScrollPos: function() {
		var x = (window.pageXOffset !== undefined) ? window.pageXOffset : (document.documentElement || document.body.parentNode || document.body).scrollLeft,
		y = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
		return {'x':x, 'y':y};
	},
	construct: function() {
		var usrAg = window.navigator.userAgent.toLowerCase();
		var appVersion = window.navigator.appVersion.toLowerCase();
		var bodyObj = document.getElementsByTagName('body')[0];
		
		if (usrAg.match(/win(dows\s)?/i) != null) {
			bodyObj.className = bodyObj.className + ' win';
			this.f_win = true;
		}
		if (usrAg.indexOf("msie") > -1) {
			this.f_ie = true;
			bodyObj.className = bodyObj.className + ' ie';
			try {
				this.brws_ver = usrAg.match(/msie\s(\d+)\./i)[1];
				bodyObj.className = bodyObj.className + ' ie_'+this.brws_ver;
			} catch(e) {}
		} else if (usrAg.toLowerCase().indexOf('trident') > -1) {
			this.f_ie = true;
			bodyObj.className = bodyObj.className + ' ie';
			try {
				this.brws_ver = usrAg.match(/trident.*\srv:(\d+)\./i)[1];
				bodyObj.className = bodyObj.className + ' ie_'+this.brws_ver;
			} catch(e) {}
		} else if (usrAg.toLowerCase().indexOf('firefox') > -1) {
			this.f_ff = true;
			bodyObj.className = bodyObj.className + ' firefox';
		} else if (usrAg.toLowerCase().indexOf('crios') > -1 || usrAg.toLowerCase().indexOf('chrome') > -1) {
			this.f_chrome = true;
			bodyObj.className = bodyObj.className + ' chrome';
		}
		if (usrAg.toLowerCase().indexOf('ipad') > -1) {
			this.f_ipad = true;
			this.f_tablet = true;
			this.f_mobile = true;
			this.f_ios = true;
			bodyObj.className = bodyObj.className + ' tablet mobile ipad';
		} else if (usrAg.toLowerCase().indexOf('iphone') > -1) {
			this.f_iphone = true;
			this.f_mobile = true;
			this.f_ios = true;
			bodyObj.className = bodyObj.className + ' smart mobile iphone';
		} else if (usrAg.toLowerCase().indexOf('android') > -1) {
			this.f_tablet = true;
			this.f_mobile = true;
			this.f_android = true;
			bodyObj.className = bodyObj.className + ' android';
			try {
				this.os_ver = usrAg.match(/android\s(\d+\.\d+)/i)[1];
				bodyObj.className = bodyObj.className + ' android_'+parseInt(this.os_ver);
			} catch(e) {}
		}
		
		// GET パラメータを取得する
		var vars = [], hash;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
		if(hashes != location.href) {
			for(var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			this.get = vars;
		}
		// IE 対策
		if(brwsInfo.f_ie) {
			if (!('console' in window)) {
				window.console = {};
				window.console.log = function(str){
					return str;
				};
			}
		}
	}
};





// ウィンドウが body より大きい時の調整
var htmlBodyHeightAdjuster = {
	f_adjust: null,
	originalHeight: false,
	fullHeightElement: false, // html, body と同じように高さを 100% にするエレメント
	adjust: function() {
		if(this.f_adjust !== null && this.originalHeight !== false) {
			if(jQuery(window).height() >= this.originalHeight ) {
				jQuery("html").addClass('adjust-height');
				jQuery("html, body").css({"height":"100%", "overflow-y":"hidden"});
				if(this.fullHeightElement !== false) { jQuery(this.fullHeightElement).css("height","100%"); }
			} else {
				jQuery("html").removeClass('adjust-height');
				jQuery("html, body").css({"height":"", "overflow-y":""});
				if(this.fullHeightElement !== false) { jQuery(this.fullHeightElement).css("height",""); }
			}
		} else {
			this.init();
		}
	},
	init: function() {
		if(screen.height <= document.body.clientHeight) { this.f_adjust = false; }
		else {
			this.f_adjust = true;
			this.originalHeight = document.body.clientHeight;
			this.adjust();
		}
		return this.f_adjust;
	}
};
htmlBodyHeightAdjuster.fullHeightElement = '';


function ieInitPropaties() {
	// IEでページを読み込んだ直後に初期設定などがうまく反映されない時にここで設定する
	// fixedMenu.topFixedDisplayViewing_resetOffset();
}

// addListener を追加する時に使用する
var addListener = (function() {
	if ( window.addEventListener ) {
		return function(el, type, fn) {
			el.addEventListener(type, fn, false);
		};
	} else if ( window.attachEvent ) {
		return function(el, type, fn) {
			var f = function() {
				fn.call(el, window.event);
			};
			el.attachEvent('on'+type, f);
		};
	} else {
		return function(el, type, fn) {
			el['on'+type] = fn;
		}
	}
})();



//! Load
jQuery(window).on('load', function(){
	
	brwsInfo.construct();
	responsibleInfo.construct();
	
	// createToTopButton();
	if(brwsInfo.f_iphone || brwsInfo.f_ipad || brwsInfo.f_android) { cnvTelLink(); }
	
	if(brwsInfo.f_ie) {
		setTimeout("ieInitPropaties();", 2);
	}
	
	// svg を png に変換する
	if(brwsInfo.f_ie && brwsInfo.brws_ver <= 9) {
		var imgSvgOb = jQuery('img[src$=".svg"]');
		for(var i=0; i < imgSvgOb.length; i++) {
			jQuery(imgSvgOb[i]).attr('src', jQuery(imgSvgOb[i]).attr('src').replace('.svg','.png'));
		}
	}
	
	
	// Bootstrap Caruosel
	jQuery('.carousel').carousel({
		interval: 6000
	});
	
	// ウィンドウが body より大きい時の調整
	if(htmlBodyHeightAdjuster.init()) {
		var timer = false;
		jQuery(window).resize(function() {
			if (timer !== false) { clearTimeout(timer); }
			timer = setTimeout(function() { htmlBodyHeightAdjuster.adjust(); }, 100);
		});
	}
});



