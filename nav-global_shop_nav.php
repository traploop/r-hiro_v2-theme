<?php
// Parts : Menu : Glogal shop nav

global $siteInfo;
?>

<?php
$gnavClass = array();
switch($siteInfo['blogSlug']) {
    case 'top': case 'overseas':
        $gnavClass2[] = 'col-sm-28';
        break;
    default: ?>
<?php
        $gnavClass2[] = 'col-sm-17';
        break;
}
?>        

<nav class="nav-global_shop_nav <?php echo implode(' ', $gnavClass2); ?>">
<?php
switch($siteInfo['blogSlug']) {
	case 'top': case 'overseas':
		get_template_part( 'global_shop_nav', 'top'); break;
	default:
		get_template_part( 'global_shop_nav', 'shops'); break;
}
?>

</nav><!-- // menu-global_shop_nav -->
