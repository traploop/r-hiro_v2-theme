<?php
global $siteInfo;


?>
		<header class="row">
			<h2 class="col-xs-28 col-sm-4"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'; ?>txt-column-top.svg" class="content-titile"></h2>
			<hr class="col-xs-28 col-sm-offset-1 col-sm-23">
			<p class="link_area  col-xs-28"><a href="/"><span class="text"><img src="<?php echo get_template_directory_uri(); ?>/images/shops/txt-group_home.svg" alt="GROUP HOME" /></span></a></p>
		</header>
		
		<div class="contents_area  row">
			<div class="contents_text_area  col-xs-28 col-sm-18 col-sm-offset-1">
				<!-- START : WP contents -->
				<h3 class="title_area"><span class="mark">●</span><span class="date"><?php echo get_the_date('Y.m.d'); ?></span><span class="title"><?php the_title(); ?></span></h3>
				<div class="contents_text">
					<?php the_content(); ?>
				</div>
				<!-- END : WP contents -->
				
				
				<div class="index_area  row"><?php
					$indexPosts = new WP_Query( array(
						'category_name' => 'columns',
						'posts_per_page' => 10,
						'order' => 'DESC',
						'orderby' => 'date',
						'post__not_in' => array($id),
					));
					
					$indexPostsHtml = '';
					if($indexPosts->have_posts()) {
						foreach($indexPosts->posts as $wPost) {
							$indexPostsHtml .= '<li><a href="'.get_permalink($wPost->ID).'"><p class="title_area"><span class="mark">●</span><span class="date title">'.date_i18n('Y.m.d', strtotime($wPost->post_date)).'</span><span class="title">'.$wPost->post_title.'</span></p></a></li>';
						}
						$indexPostsHtml = '<ul class="columns_list col-xs-28">'.$indexPostsHtml.'</ul>';
					}
					
					echo $indexPostsHtml;
?>
				</div>
			</div>
			<div class="image_area  col-xs-28 col-sm-7 col-sm-offset-1">
				<figure><img class="img-responsive" src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>img-kazushige_yamaguchi.jpg"></figure>
				<p class="name"><span class="text"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>txt-kazushige_yamaguchi.svg"></span></p>
				
				
			</div><!-- // .image_area -->
		</div>
