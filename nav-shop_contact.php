<?php
// Parts : Menu : Glogal shop nav

global $siteInfo;

$siteTel = esc_html(get_option('site_tel'));
$siteFb = stripslashes(esc_html(get_option('site_facebook')));
$siteOt = stripslashes(esc_html(get_option('site_opentable')));



switch($siteInfo['blogSlug']) {
	case 'top': case 'overseas': break;
	default: ?>
		<div class="contact_info col-xs-11 col-sm-11 pull-right"><?php
		if(!empty($siteOt)) { ?>
			<p class="icon_opentable"><a href="<?php echo $siteOt; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/icon-opentable.png" alt="open table"></a></p>		
<?php
		} 
		if(!empty($siteFb)) { ?>
			<p class="icon_facebook"><a href="<?php echo $siteFb; ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/icon-fb.svg" alt="facebook"></a></p>		
<?php
		} 
		if(!empty($siteTel)) { ?>
			<p class="reservation"><img class="svg" src="<?php echo get_template_directory_uri().'/images/'.$siteInfo['blogSlug'].'/txt-tel.svg' ?>" alt="Reservation : <?php echo $siteTel; ?>"></p>
<?php
		} ?>
		</div><!-- // .contact_info -->
<?php
		break;
}
