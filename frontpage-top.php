<?php
// Parts : Front page : Top

global $siteInfo;

wp_enqueue_script('bxslider');
wp_enqueue_style('bxslider');




get_header(); ?>


<article <?php post_class(); ?>>
	<?php get_template_part( 'content', 'home_slide'); ?>
	
	<?php get_template_part( 'content', 'all_site_topics'); ?>
	
	<div class="inner-wrapper">
		<div class="welcome">
			<p class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/common/txt-welcome.svg" alt="Welcome" /></p>
			<div class="contents">
				<p>“Japanese Italian”の先駆けと自負する我々リストランテ・ヒロは、お食事の嬉しい・楽しい・大好きを一皿一皿に添えてお届けします。オープンから20年、青山から始まった店も現在、東京４店舗、大阪１店舗となりました。各地5人のシェフの個性あふれる料理と5つの特徴ある空間で皆様をお迎えいたします。四季折々の食材とイタリアを愛する者たちのエッセンスが、新鮮な驚きと美味しさを作り上げます。リストランテ・ヒロは、店舗によってメニューが異なり席数も様々。ご利用目的やご予算に合ったお店選びが可能です。私たちのレストランが、お客様のお気に入りの1店舗になることを願って、皆さまのお越しを心よりお待ちしております。</p>
			</div>
		</div>
		
		
		<section class="column_area  row">
			<header class="col-xs-28 col-sm-28">
				<figure class="col-sm-28"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/common/txt-ristorante_hiro_column.svg" alt="txt-ristrante_hiro_column" /></figure>
				<hr class="col-xs-28 col-sm-28">
			</header>
			<div class="home_news  hidden-xs col-sm-14"><?php get_template_part( 'content', 'facebook'); ?></div>
			
			<div class="manager_profile_area col-sm-14">
				<div class="photo_area"><div class="row">
					<figure class="manager_photo  col-xs-28 col-sm-16"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/top/img-yamaguchi_kazuhige.jpg" alt="img-yamaguchi_kazuhige" /></figure>
					<div class="company_info  col-xs-28 col-sm-12">
						<figure class="logo"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/top/img-logo-wb1.svg" alt="img-logo-wb1" /></figure>
						<div class="company_info_text">
							<figure><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/images/top/txt-company1.svg" alt="txt-company1" /></figure>
							<div class="hidden">
								<p class="company_name">株式会社リストランテ・ヒロ</p>
								<p class="division"><span class="div_name">代表取締役社長</span>　<span class="name">山口 一樹</span></p>
								<p class="ruby">KAZUSHIGE YAMAGUCHI</p>
							</div>
						</div>
					</div>
				</div></div><!-- // .photo_area -->
				
				<div class="profile">
					<p>1972年静岡県生まれ。<br>日本におけるイタリア料理のパイオニア山田宏巳氏と出会い共に、神奈川東京のイタリアンレストランに勤務する。1995年 株式会社リストランテ•ヒロ設立メンバーとして立ち上げに参加。その後、代官山出店時には統括マネージャーを務め、丸ビル出店にあたり役員に就任。2006年 代表取締役社長となる。現在も各店を回り現役サービスマンとしても活動を続けている。</p>
					<p>下記のコラムとFacebookを通して、リストランテ・ヒロの今と山口の今を発信して行きます。</p>
				</div>
				
				<div class="columns">
					<p class="title"><img src="<?php echo get_template_directory_uri(); ?>/images/common/txt-column.svg" alt="Column" /></p>
					<ul class="column_list"><?php
						$moreLink = '';
						$query = array(
							//'posts_per_page' => 0,
							'category_name' => 'columns',
							'orderby' => 'post_date',
							'order' => 'DESC',
							'posts_per_page' => $siteInfo['blogInfoList']['top']['home']['columns_post'],
						);
						$rec = new WP_Query($query);
						
						if($rec->have_posts()) {
							$moreLink = get_permalink($rec->posts[0]->ID);
							foreach($rec->posts as $recPost) {
								$postId = $recPost->ID;
								// $anc = get_permalink( $postId );
								$dateStr = date_i18n('Y.m.d', strtotime($recPost->post_date));
								$titleStr = $recPost->post_title;
								echo '<li id="columns-post-'.$postId.'" date-postid="'.$postId.'" onclick="rhiro_displayModal_post('.$postId.');"><span class="date">'.$dateStr.'</span><span class="post_title">'.$titleStr.'</span></li>';
							}
						}
					?></ul>
					<p class="link_area"><a href="<?php echo $moreLink; ?>">More <i class="fa fa-angle-double-right"></i></a></p>
				</div><!-- // .column -->
				
			</div><!-- // .manager_profile_area -->
		</section>
	</div>
</article>
<script>constructDialog();</script>
<?php get_footer();
