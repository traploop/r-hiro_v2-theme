<?php
// Parts : Global shop nav : Shops

global $blog_id, $siteInfo;

?>
	<div class="navbar navbar-default navbar-static col-xs-2 col-xs-offset-1 pull-right">
		<div class="container-fluid">
    		<div class="navbar-header">
	        	<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target=".global-nav-toggle">
	            	<span class="sr-only">Toggle navigation</span>
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
	            	<span class="icon-bar"></span>
	        	</button>
			</div>
		</div>
	</div>
</div>

<div class="global_shop_nav-shops-xs col-xs-28 navbar-collapse global-nav-toggle collapse" aria-expanded="true">
	<ul class="row"><?php
		foreach($siteInfo['blogInfoList'] as $wBlogKey => $wBlog) {
			switch($wBlogKey) {
				case 'top': break;
				default:
				$class = array($wBlogKey);
				if($wBlogKey == $siteInfo['blogSlug']) { $class[] = 'active'; }
		?>
		<li class="col-xs-28 ">
			<a href="<?php echo $wBlog['url']; ?>">
				<span class="text"><?php echo $wBlog['name']; ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class="text"><?php echo $wBlog['name_jp']; ?></span>
			<i class="fa fa-angle-double-right"></i></a>
		</li>
<?php
			break;
	}
}
?>
	</ul>
	
</div><!-- // .global_shop_nav-shops -->
