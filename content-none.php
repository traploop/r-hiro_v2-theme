<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title">検索されたキーワードで見つかりませんでした。</h1>
	</header><!-- .page-header -->

	<div class="page-content">

		<p>検索したキーワードでページが見つかりませんでした。</p>

		<?php get_search_form(); ?>

	</div><!-- .page-content -->
</section><!-- .no-results -->
