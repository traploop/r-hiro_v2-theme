<?php
// Parts : Global shop nav : Shops

global $blog_id, $siteInfo;

?>

<div class="global_shop_nav-shops col-sm-28">
	<ul class="row"><?php
foreach($siteInfo['blogInfoList'] as $wBlogKey => $wBlog) {
	switch($wBlogKey) {
		case 'top': break;
		default:
			$class = array($wBlogKey);
			if($wBlogKey == $siteInfo['blogSlug']) { $class[] = 'active'; }
?>
		<li class="<?php echo implode(' ', $class); ?> col-sm-4"><a href="<?php echo $wBlog['url']; ?>">
			<p class="name"><span class="text"><?php echo $wBlog['name']; ?></span></p>
			<p class="subname"><span class="text"><?php echo $wBlog['name_jp']; ?></span></p>
		</a></li>
<?php
			break;
	}
}
?>
	</ul>

</div><!-- // .global_shop_nav-shops -->
