<?php
/**
 * The template for displaying all single posts and attachments
 *
 */

/*
if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}
*/
if(in_category('columns') && file_exists(get_template_directory().'/css/single-category-columns.css')) {
	wp_enqueue_style( 'single-category-columns', get_template_directory_uri() . '/css/single-category-columns.css' );
} else {
	the_content();
}

get_header(); ?>

<article <?php post_class("content-area"); ?>>
	<main id="main" class="site_contents  inner-wrapper" role="main">

	<?php
	
	if ( have_posts() ) : the_post();
		
		if(in_category('columns')) {
			get_template_part( 'single', 'category-columns');
		} else {
			the_content();
		}
		

	endif;
	?>

	</main><!-- .site-main -->
</article>

<?php get_footer();

