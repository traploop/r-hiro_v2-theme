<?php
/**
 * The template for displaying the footer
 *
 */
?>

</div><!-- // .wrapper -->

<footer class="site-footer row"><div class="wrapper">
	<!-- XS-size navigation -->
	<nav id="nav-footer_menu-xs" class="site-nav visible-xs" role="navigation">
		<ul>
			<li class="shop_guide  list-header">
				<div id="accordion" class="panel-group"  role="tablist" aria-multiselectable="true">
	  				<div class="panel panel-default">
	    				<div class="panel-heading" role="tab" id="headingOne">
	      					<h4 class="panel-title">
	        					<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
	          						店舗案内<i class="fa fa-angle-down"></i></a>
	        					</a>
	      					</h4>
	    				</div>
	    				<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	      					<div class="panel-body">
	      					<ul>
	      						<li><a href="/aoyama/"><span class="text">リストランテ・ヒロ 青山</span><i class="fa fa-angle-right"></i></a></li>
	      						<li><a href="/centro/"><span class="text">リストランテ・ヒロ チェントロ 丸ビル</span><i class="fa fa-angle-right"></i></a></li>
	      						<li><a href="/primo/"><span class="text">ヒロ プリモ 東京駅</span><i class="fa fa-angle-right"></i></a></li>
	      						<li><a href="/ginza/"><span class="text">リストランテ・ヒロ 銀座</span><i class="fa fa-angle-right"></i></a></li>
	      						<li><a href="/osaka/"><span class="text">リストランテ・ヒロ 大阪</span><i class="fa fa-angle-right"></i></a></li>
	      						<!--li><a href="/hakata/"><span class="text">リストランテ・ヒロ 博多</span><i class="fa fa-angle-right"></i></a></li-->
	      					</ul>
							</div>
	    				</div>
	 			 	</div>
				</div>
			</li>
			<li class="recruit  list-header"><a href="/recruit">求人案内<i class="fa fa-angle-right"></i></a></li>
			<li class="privacy_policy  list-header"><a href="/privacy_policy">プライバシーポリシー<i class="fa fa-angle-right"></i></a></li>
			<li class="contact_us  list-header"><a href="mailto:info@r-hiro.com">お問合せ・ご意見メール<i class="fa fa-angle-right"></i></a></li>
			<li class="sitemap  list-header"><a href="/sitemap">サイトマップ<i class="fa fa-angle-right"></i></a></li>
		</ul>
	</nav>

	<!-- /XS-size navigation -->
	<!-- over SM-size navigation -->
	<nav id="nav-footer_menu" class="site-nav hidden-xs" role="navigation"><ul>
		<li><a href="/">ヒログループ総合ページ</a></li>
		<li><a href="/recruit">求人案内</a></li>
		<li><a href="/privacy_policy">プライバシーポリシー</a></li>
		<li><a href="mailto:info@r-hiro.com">お問合せ・ご意見メール</a></li>
		<li><a href="/sitemap">サイトマップ</a></li>
	</ul></nav><!-- .main-navigation -->
	
	<div class="site-info  hidden-xs">
		<figure class="japan_map"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img-footer_map.svg"></figure>
		
		<nav id="fmap_area_nav" class="hidden-xs">
			<div class="area_item tokyo" data-area="tokyo"><span class="text">TOKYO</span><span class="mark"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img-circle-red-lg.svg" alt="img-circle-red-lg" /></span></div>
			<div class="area_item osaka" data-area="osaka"><span class="text">OSAKA</span><span class="mark"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img-circle-red-sm.svg" alt="img-circle-red-sm" /></span></div>
			<!--div class="area_item fukuoka" data-area="fukuoka"><span class="text">FUKUOKA</span><span class="mark"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img-circle-red-sm.svg" alt="img-circle-red-sm" /></span></div-->
		</nav><!-- // #fmap_area_nav -->
		
		
		<div id="fmap_list_nav" class="hidden-xs"><ul>
			<li class="aoyama area-tokyo" data-area="tokyo">
				<div class="place"><p class="eng"><span class="text">AOYAMA</span></p><p class="jpn"><span class="text">青山</span></p></div>
				<div class="info">
					<p class="name"><a href="/aoyama/"><span class="text">リストランテ・ヒロ 青山</span></a></p>
					<p class="tel"><span class="text">Tel 03 3486 5561</span></p>
					<p class="address"><span class="text">東京都港区南青山5-5-25　T-PLACE B1</span></p>
				</div>
			</li><!-- // .aoyama -->
			<li class="centro area-tokyo" data-area="tokyo">
				<div class="place"><p class="eng"><span class="text">CENTRO</span></p><p class="jpn"><span class="text">丸ビル</span></p></div>
				<div class="info">
					<p class="name"><a href="/centro/"><span class="text">リストランテ・ヒロ チェントロ 丸ビル</span></a></p>
					<p class="tel"><span class="text">Tel 03 5221 8331</span></p>
					<p class="address"><span class="text">東京都千代田区丸の内2-4-1 丸の内ビルディング35F</span></p>
				</div>
			</li><!-- // .centro -->
			<li class="primo area-tokyo" data-area="tokyo">
				<div class="place"><p class="eng"><span class="text">PRIMO</span></p><p class="jpn"><span class="text">東京駅</span></p></div>
				<div class="info">
					<p class="name"><a href="/primo/"><span class="text">ヒロ プリモ 東京駅</span></a></p>
					<p class="tel"><span class="text">Tel 03 5219 5607</span></p>
					<p class="address"><span class="text">東京都千代田区丸の内1-9-1 東京駅1Fキッチンストリート内</span></p>
				</div>
			</li><!-- // .primo -->
			<li class="ginza area-tokyo" data-area="tokyo">
				<div class="place"><p class="eng"><span class="text">GINZA</span></p><p class="jpn"><span class="text">銀座</span></p></div>
				<div class="info">
					<p class="name"><a href="/ginza/"><span class="text">リストランテ・ヒロ 銀座</span></a></p>
					<p class="tel"><span class="text">Tel 03 3535 2750</span></p>
					<p class="address"><span class="text">東京都中央区銀座3-2-15 ギンザ・グラッセ 7F</span></p>
				</div>
			</li><!-- // .ginza -->
			<li class="osaka area-osaka" data-area="osaka">
				<div class="place"><p class="eng"><span class="text">OSAKA</span></p><p class="jpn"><span class="text">大阪</span></p></div>
				<div class="info">
					<p class="name"><a href="/osaka/"><span class="text">リストランテ・ヒロ 大阪</span></a></p>
					<p class="tel"><span class="text">Tel 06 6343 6966</span></p>
					<p class="address"><span class="text">大阪府大阪市北区梅田2-4-9ブリーゼブリーゼ5F</span></p>
				</div>
			</li><!-- // .osaka -->
			<!--li class="hakata area-fukuoka" data-area="fukuoka">
				<div class="place"><p class="eng"><span class="text">HAKATA</span></p><p class="jpn"><span class="text">博多</span></p></div>
				<div class="info">
					<p class="name"><a href="/hakata/"><span class="text">リストランテ・ヒロ 博多</span></a></p>
					<p class="tel"><span class="text">Tel 092 409 9061</span></p>
					<p class="address"><span class="text">福岡県福岡市博多区中洲3-7-24 gate's 1F</span></p>
				</div>
			</li--><!-- // .hakata -->
		</ul></div><!-- // #fmap_list_nav -->
	</div><!-- .site-info -->
	<small class="copyright col-xs-28"><span class="text">copyright © Ristorante HiRo All Rights Reserved.</span></small>
</div></footer><!-- .site-footer -->
<script>
// ! Footer Map
// Area Hover
jQuery('#fmap_area_nav .area_item').children('.text, .mark').hover(function() {
	// Hover
	var area = jQuery(this).parent().data('area');
	jQuery(this).parent().addClass('active');
	jQuery('#fmap_list_nav .area-'+area).addClass('active');
}, function() {
	var area = jQuery(this).parent().data('area');
	jQuery(this).parent().removeClass('active');
	jQuery('#fmap_list_nav .area-'+area).removeClass('active');
});

// Shops list Hover
jQuery('#fmap_list_nav li').hover(function() {
	// Hover
	var area = jQuery(this).data('area');
	jQuery(this).addClass('active');
	jQuery('#fmap_area_nav .'+area).addClass('active');
}, function() {
	var area = jQuery(this).data('area');
	jQuery(this).removeClass('active');
	jQuery('#fmap_area_nav .'+area).removeClass('active');
});


// ! XS nav


jQuery('#nav-footer_menu-xs .list-header .panel-group .panel-heading a').click(function() {
	// トグルアイコンの切り替え処理
	var clickMnOb = jQuery(this).find('.fa');
	if(jQuery(clickMnOb).hasClass('fa-angle-down')) {
		jQuery(clickMnOb).addClass('fa-angle-up').removeClass('fa-angle-down');
	} else {
		jQuery(clickMnOb).addClass('fa-angle-down').removeClass('fa-angle-up');
	}
});
</script>

<?php wp_footer(); ?>

<div class="modal"></div></body>
</html>
