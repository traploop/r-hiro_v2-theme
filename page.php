<?php
/**
 * 固定ページ
 */

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}





get_header(); ?>


<article <?php post_class('page-'.$name); ?>>
<?php if( have_posts() ) : the_post(); ?>
	<div class="site_contents">
		<?php
			the_content();
		?>
	</div>
		
<?php endif; ?>
</article>

<?php get_footer();

get_header(); ?>

