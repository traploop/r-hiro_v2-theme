<?php
/*
 Page : dinner
 
*/
global $siteInfo;

$siteInfo['xs__site_page_nav'] = false;


if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}

get_header(); 


?>

<article <?php post_class('page-'.$name); ?>>
<?php if( have_posts() ) : the_post(); ?>
	<!--<figure class="bg_image"><img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /></figure>-->
	
	<div class="site_contents tempo_bg">
		<div class="row">
			<div class="col-xs-28 col-sm-14">
				<!-- start carousel -->
				<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
  					<ol class="carousel-indicators">
    					<li data-target="#carousel" data-slide-to="0" class="active"></li>
    					<li data-target="#carousel" data-slide-to="1"></li>
    					<li data-target="#carousel" data-slide-to="2"></li>
  					</ol>
  					<!-- Carousel items -->
  					<div class="carousel-inner">
  					  <div class="active item"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'.$name; ?>_img1.jpg" class="img-responsive"></div>
  					  <div class="item"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'.$name; ?>_img2.jpg" class="img-responsive"></div>
  					  <div class="item"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'.$name; ?>_img3.jpg" class="img-responsive"></div>
  					</div>
  					<!-- Carousel nav -->
  					<!--<a class="carousel-control left" href="#carousel" data-slide="prev">&lsaquo;</a>
  					<a class="carousel-control right" href="#carousel" data-slide="next">&rsaquo;</a>-->
				</div><!-- end carousel -->
			</div>

			<?php get_template_part( 'nav', 'site-page_nav-xs'); ?>
			
			<div class="col-xs-28 col-sm-14 contents-body">
				<div class="inner-wrapper dinner-text">
					<h2><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'.$name ?>_title.svg" class="content-titile"><span><?php
							echo esc_html(get_option("site_business_time_dinner")); ?></span></h2>
					<!-- START : WP contents -->
					
					<!-- END : WP contents -->
					<?php
						the_content();
					?>
				</div>
			</div>
		</div>
	</div>
		
<?php endif; ?>
</article>
<?php get_footer();
