<?php
// Parts : Menu : Glogal shop nav

global $siteInfo;
?>

<?php
$gnavClass = array();
switch($siteInfo['blogSlug']) {
    case 'top': case 'overseas':
        $gnavClass2[] = 'col-xs-2 col-xs-offset-16';
        break;
    default: ?>
<?php
        $gnavClass2[] = 'col-xs-2 col-xs-offset-16';
        break;
}
?>        

<div class="<?php echo implode(' ', $gnavClass2); ?>">
<?php
switch($siteInfo['blogSlug']) {
	case 'top': case 'overseas':
		get_template_part( 'global_shop_nav', 'top-xs'); break;
	default:
		get_template_part( 'global_shop_nav', 'shops-xs'); break;
}
?>
<!-- // menu-global_shop_nav -->
