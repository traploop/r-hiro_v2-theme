<?php
/*
 Page : Information
 
*/

global $siteInfo;

get_header(); 

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}


?>

<article <?php post_class('page-'.$name); ?>>
<?php
query_posts( array(
	'category_name' => 'information',
	'posts_per_page' => 10,
	'order' => 'DESC',
	'orderby' => 'date',
));

if( have_posts() ) : ?>
	<div class="site_contents row tempo_bg">
		<div class="hidden-xs bg-image"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>information_bg.jpg"></div>
		<div class="col-xs-28 row contents-body">
			<div class="col-xs-28">
				<div class="inner-wrapper information-text  col-xs-28 col-sm-18 col-sm-offset-2">
					<h2 class="col-sm-28"><img src="<?php echo '/wp-content/themes/ristrante_hiro/images/common/'; ?>txt-information-gray.svg" class="content-titile"></h2>
					<!-- START : WP contents -->
					<div class="information_list col-sm-28"><ul id="scroll-info_area">
<?php
	while( have_posts() ) : the_post(); ?>
						<li class="col-sm-28">
							<p class="title_area"><span class="date"><?php echo get_the_date('Y.m.d'); ?></span><span class="title"><?php the_title(); ?></span></p>
							<div class="information-contents">
<?php
		the_content();
?>
							</div>
						</li>
<?php
	endwhile;
?>
					</ul></div>
					<!-- END : WP contents -->
					<div class="button_area  row hidden-xs">
						<div id="view-scroll-up" class="button_item  col-sm-offset-18 col-sm-5"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn-arrow-up.svg" alt="btn-arrow-up" /></div>
						<div id="view-scroll-down" class="button_item  col-sm-5"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn-arrow-down.svg" alt="btn-arrow-down" /></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		
		var viewScrollObj = {
			ob: jQuery('#scroll-info_area'),
			stopCount: 0,
			stopMax: 3,
			type: false,
			timer: false,
			'move': 120,
			'speed': 250,
			'interval': 600
		};
		
		function viewClickScroll(type) {	// クリックでスクロールする際に使用する
			if(type === undefined) { return false; }
			if(viewScrollObj.timer === false) {
				viewScrollObj.type = type;
				var mvPos = jQuery(viewScrollObj.ob).scrollTop();
				if(viewScrollObj.type === 'up') {
					mvPos -= viewScrollObj.move;
				} else {
					mvPos += viewScrollObj.move;
				}
				jQuery(viewScrollObj.ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing');
				
			}
		}
		function viewHoverScroll(type) {	// ホバーでスクロールする際に使用する
			if(type === undefined) { return false; }
			if(viewScrollObj.timer === false) {
				viewScrollObj.type = type;
				var mvPos = jQuery(viewScrollObj.ob).scrollTop();
				if(viewScrollObj.type === 'up') {
					mvPos -= viewScrollObj.move;
				} else {
					mvPos += viewScrollObj.move;
				}
				jQuery(viewScrollObj.ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing');
				
				viewScrollObj.timer = setInterval(function() {
					var ob = viewScrollObj.ob;
					var start = jQuery(ob).scrollTop();
					if(viewScrollObj.type === 'up') {
						var mvPos = (start - viewScrollObj.move);
						var nowPos = jQuery(ob).scrollTop();
						jQuery(ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing', function() {
							if(nowPos == jQuery(this).scrollTop()) {
								if(viewScrollObj.stopCount < viewScrollObj.stopMax) {
									viewScrollObj.stopCount++;
								} else {
									clearViewScroll();
									viewScrollObj.stopCount = 0;
								}
							}
						});
					} else {
						var mvPos = (start + viewScrollObj.move);
						var nowPos = jQuery(ob).scrollTop();
						jQuery(ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing', function() {
							if(nowPos == jQuery(this).scrollTop()) {
								if(viewScrollObj.stopCount < viewScrollObj.stopMax) {
									viewScrollObj.stopCount++;
								} else {
									clearViewScroll();
									viewScrollObj.stopCount = 0;
								}
							}
						});
					}
				}, viewScrollObj.interval);
			}
		}
		
		function clearViewScroll() {
			if(viewScrollObj.timer !== false) {
				clearInterval(viewScrollObj.timer);
				viewScrollObj.timer = false;
			}
		}
		/*
		jQuery("#view-scroll-up").click(function() {
			viewClickScroll('up');
		});
		jQuery("#view-scroll-down").click(function() {
			viewClickScroll('down');
		});
		*/
		jQuery("#view-scroll-up").hover(function() {
			viewHoverScroll('up');
		}, function() {
			clearViewScroll();
		});
		jQuery("#view-scroll-down").hover(function() {
			viewHoverScroll('down');
		}, function() {
			clearViewScroll();
		});
	</script>
<?php
endif;
wp_reset_query();
?>
</article>
<?php get_footer();
