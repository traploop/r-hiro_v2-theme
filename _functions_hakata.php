<?php
/**
 テーマ内の関数や設定をまとめておく
 */

// Set the content width based on the theme's design and stylesheet.
// 参考：　http://wpdocs.sourceforge.jp/Content_Width
// モバイルを入れてレスポンシブで表示させるので、幅の設定はできないかも。
/*
if ( ! isset( $content_width ) ) {
	$content_width = 1280;
}
*/

global $blog_id, $siteInfo;

// Setup site info
$siteInfo = array(
	'blogSlug' => getBlogSlug(),
	'blogInfoList' => array(
		'top' => array(
			'name' => 'GROUP HOME',
			'name_jp' => '総合トップ',
			'home' => array(
				'columns_post' => 1,
			),
		),
		'aoyama' => array(
			'name' => 'AOYAMA',
			'name_jp' => '青山',
		),
		'centro' => array(
			'name' => 'CENTRO',
			'name_jp' => '丸ビル',
		),
		'primo' => array(
			'name' => 'PRIMO',
			'name_jp' => '東京駅',
		),
		'ginza' => array(
			'name' => 'GINZA',
			'name_jp' => '銀座',
		),
		'osaka' => array(
			'name' => 'OSAKA',
			'name_jp' => '大阪',
		),
		'hakata' => array(
			'name' => 'HAKATA',
			'name_jp' => '博多',
		),
	),
	'facebook_url' => 'https://www.facebook.com/RISTORANTE.HiRo.aoyama',
	'topics_num' => 4,
	'xs__site_page_nav' => true,
);
$wSiteDatList = wp_get_sites();
if(is_array($wSiteDatList)) {
	foreach($wSiteDatList as $siteDat) {
		$siteSlug = getBlogSlug($siteDat['path']);
		if(isset($siteInfo['blogInfoList'][$siteSlug])) {
			$siteInfo['blogInfoList'][$siteSlug]['data'] = $siteDat;
			$siteInfo['blogInfoList'][$siteSlug]['url'] = '//'.$siteDat['domain'].$siteDat['path'];
		}
	}
}


remove_action('wp_head', 'wp_generator');


wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css', array('bootstrap'), '0.1.1');

if(!is_admin()) {
	wp_enqueue_script( 'common-js', get_template_directory_uri() . '/js/common.js', array('jquery'));
} else {
	wp_enqueue_script( 'admin', get_template_directory_uri() . '/js/admin.js', array('jquery'));
	wp_enqueue_style( 'admin', get_template_directory_uri() . '/css/admin.css', array(), '0.1' );
	if(file_exists(get_template_directory().'/css/site-'.$siteInfo['blogSlug'].'-admin.css')) {
		wp_enqueue_style( 'site-'.$siteInfo['blogSlug'].'-admin', get_template_directory_uri() . '/css/site-'.$siteInfo['blogSlug'].'-admin.css', array(), '0.1.1' );
	}
	if(file_exists(get_template_directory().'/js/site-'.$siteInfo['blogSlug'].'-admin.js')) {
		wp_enqueue_script( 'site-'.$siteInfo['blogSlug'].'-admin', get_template_directory_uri() . '/js/site-'.$siteInfo['blogSlug'].'-admin.js', array(), '0.1.1' );
	}
}
//wp_enqueue_script( 'bootstrap-modal', get_template_directory_uri() . '/js/bootstrap/modal.js', array('jquery', 'bootstrap'), '0.1', true );

switch($siteInfo['blogSlug']) {
	case 'top': break;
	default:
		wp_enqueue_style( 'site_style-shops', get_template_directory_uri() . '/css/site_style-shops.css', array('style'), '0.1.2' );
		break;
}
if(file_exists(get_template_directory().'/css/site-'.$siteInfo['blogSlug'].'.css')) {
	wp_enqueue_style( 'site-'.$siteInfo['blogSlug'], get_template_directory_uri() . '/css/site-'.$siteInfo['blogSlug'].'.css', array('style'), '0.1.1' );
}
if(file_exists(get_template_directory().'/js/site-'.$siteInfo['blogSlug'].'.js')) {
	wp_enqueue_script( 'site-'.$siteInfo['blogSlug'], get_template_directory_uri() . '/js/site-'.$siteInfo['blogSlug'].'.js', array('common'), '0.1.1' );
}


wp_register_style( 'bxslider', get_template_directory_uri() . '/js/bxslider/jquery.bxslider.css', array() );
wp_register_script( 'bxslider', get_template_directory_uri() . '/js/bxslider/jquery.bxslider-wp.js', array('jquery') );






// Sets up theme defaults and registers support for various WordPress features.
function rhiro_themeSetup() {


	// headerでの、投稿とコメントのRSSフィードのリンクを有効にする
	add_theme_support( 'automatic-feed-links' );

	// HTML <head> にドキュメントタイトルタグを追加できるようになる
	add_theme_support( 'title-tag' );
	
	
	// 投稿サムネイル
	// オプションとして、この機能を有効にしたい投稿タイプの配列を第2引数で渡すことができる
	// 参考：　http://wpdocs.sourceforge.jp/%E9%96%A2%E6%95%B0%E3%83%AA%E3%83%95%E3%82%A1%E3%83%AC%E3%83%B3%E3%82%B9/add_theme_support
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
	
	
	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'globalMenu' => 'グローバルメニュー',
		// 'social'  => 'SNSボタン',
	) );

	// コメントフォーム、検索フォーム、およびコメントリストでHTML5マークアップの使用を許可
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );

	// 投稿フォーマット
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );

	// カスタム背景
	// サイトの背景色や背景画像をダッシュボードから自由に変更できるようにする
	// 第2引数でデフォルトの引数を使用できる
	// $color_scheme  = twentyfifteen_get_color_scheme();
	// $default_color = trim( $color_scheme[0], '#' );

	// add_theme_support( 'custom-background', apply_filters( 'twentyfifteen_custom_background_args', array(
	// 	'default-color'      => $default_color,
	// 	'default-attachment' => 'fixed',
	// ) ) );
	
	
	// カスタムヘッダー
	// サイトに表示する画像のうち１カ所だけ、ダッシュボードから好きな画像に変更できるようにする
	// 第2引数でデフォルトの引数を使用できる
	// カスタムヘッダー画像を設置する
	$custom_header_defaults = array(
			'default-image'          => get_bloginfo('template_url').'/images/header/img_header-dmy-01.jpg',
			'width'                  => 1280,
			'height'                 => 489,
			'header-text'            => false,	//ヘッダー画像上にテキストをかぶせる
	);
	add_theme_support( 'custom-header', $custom_header_defaults );	// add_theme_support( 'custom-header' );
	
	

	// カスタム TinyMCE エディタスタイルシートのコールバックを追加
	add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css') );
}
add_action( 'after_setup_theme', 'rhiro_themeSetup' );




// Register widget area.
function rhiro_admin_print_scripts() {
	global $siteInfo;
?>	
<script>
	var siteInfo = {
		blogSlug: '<?php echo $siteInfo['blogSlug']; ?>'
	}
</script>
<?php	
}
add_action( 'admin_print_scripts', 'rhiro_admin_print_scripts' );


// Enqueue scripts and styles.
function rhiro_enqueueScripts() {
	// Load our main stylesheet.
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css' );
	
	
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'ie', get_template_directory_uri() . '/css/ie.css', array( 'style' ), '20141010' );
	wp_style_add_data( 'ie', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'ie7', get_template_directory_uri() . '/css/ie7.css', array( 'style' ), '20141010' );
	wp_style_add_data( 'ie7', 'conditional', 'lt IE 8' );

}
add_action( 'wp_enqueue_scripts', 'rhiro_enqueueScripts' );



function rhiro_wp_head() {
	global $siteInfo, $name;
	
	if(!is_admin()) {
		wp_print_scripts( array( 'sack' ));
		if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
			wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css', array('style', 'site-'.$siteInfo['blogSlug']), '0.1.1' );
		}
		if(file_exists(get_template_directory().'/css/font-awesome/font-awesome.css')) {
			wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/font-awesome/font-awesome.css', array('style', 'site-'.$siteInfo['blogSlug']), '0.1.1' );
		}
		if(file_exists(get_template_directory().'/js/page-'.$name.'.js')) {
			wp_enqueue_script( 'page-'.$name, get_template_directory_uri() . '/js/page-'.$name.'.js', array('common', 'site-'.$siteInfo['blogSlug']), '0.1.1' );
		}
	}
?>
	<script>
		var siteInfo = { 'ajax': {} };
		function get_bloginfo(arg) {
			switch (arg) {
				case 'wpurl': return "<?php bloginfo('wpurl'); ?>";
				case 'ajaxurl': '<?php echo admin_url( 'admin-ajax.php'); ?>';
			}
		}
	</script>
<?php
}
add_action( 'wp_head', 'rhiro_wp_head', 1 );




// ! Utility ===========================================================================
function getBlogSlug($path='') {
	$res = '';
	if(empty($path)) {
		global $blog_id;
		return getBlogSlug(get_blog_status( $blog_id, 'path' ));
	}
	$res = trim( $path, '/' );
	if(empty($res)) { $res = 'top'; }
	return $res;
}


// Topics の新しいトピックスの画像オブジェクトを取得する
function rhiro_getNewTopicsImage() {
	$res = false;
	$resTopics = new WP_Query(array(
		'category_name' => 'topics',
		'post_per_page' => 1,
		'order' => 'DESC',
		'orderby' => 'date',
	));
	if($resTopics->have_posts()) {
		$res = get_field('content_image', $resTopics->posts[0]->ID);
	}
	return $res;
}



// PHP の値をシリアライズするための関数
// 通常の serialize だけではうまく復元できないため、この関数を使う
function rhiro_serializeValiable($val) {	// new ver 0.1.1.1
	$serVal = $val;
	return str_replace(
		array(
			'"',"'","\n","\r",'\\',
		),
		array(
			'__=EP_DQT_EP=__','__=EP_SQT_EP=__','__=EP_RTN_EP=__','__=EP_CRTN_EP=__','__=EP_BKSL_EP=__',
		), $serVal
	);
}
function rhiro_unserializeValiable($val) {	// new ver 0.1.1.1
	$repVal = str_replace(
		array(
			'__=EP_DQT_EP=__','__=EP_SQT_EP=__','__=EP_RTN_EP=__','__=EP_CRTN_EP=__','__=EP_BKSL_EP=__',
		),
		array(
			'"',"'","\n","\r",'\\',
		), $val
	);
	if(empty($resVal)) { return $val; }
	return $resVal;
}




// ! Ajax ===========================================================================
function rhiro_ajax_displayModal_blogPost(){
	global $siteInfo;
	$postId = $_POST['postId'];
	$blogSlug = $_POST['blogSlug'];
	
	$res = '';
	// スクリプトを生成
	$endScript = '';
	
	global $posts;
	global $wp_query;
	
	query_posts(array(
		'p' => $postId,
	));
	//$endScript .= '/*'.var_export($resPosts, true).'*/';
	$endScript .= '/*'.var_export($wp_query->have_posts(), true).'*/';
	
	if ( $wp_query->have_posts() ) {
		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			$dat = array(
				'id' => get_the_ID(),
				'title' => get_the_title(),
				'content' => get_the_content(),
			);
			$res = getHtml_modal_recruitPost($dat);
			
			$endScript .= 'siteInfo.ajax.getRecruitPost_top'.$postId.' = "'. rhiro_serializeValiable($res['html']).'";';
			$endScript .= 'createDialog(rhiro_unserializeValiable(siteInfo.ajax.getRecruitPost_top'.$postId.'), "'.$res['id'].'");';
			$endScript .= 'openDialog("'.$res['id'].'");';
		}
	}
		
	die($endScript);
}
add_action( 'wp_ajax_rhiro_ajax_displayModal_blogPost', 'rhiro_ajax_displayModal_blogPost' );
add_action( 'wp_ajax_nopriv_rhiro_ajax_displayModal_blogPost', 'rhiro_ajax_displayModal_blogPost' );


function rhiro_ajax_displayModal_post(){
	global $siteInfo;
	$postId = $_POST['postId'];
	
	$res = '';
	// スクリプトを生成
	$endScript = '';
	
	global $posts;
	global $wp_query;
	
	query_posts(array(
		'p' => $postId,
	));
	$endScript .= '/*'.var_export($wp_query->have_posts(), true).'*/';
	
	if ( $wp_query->have_posts() ) {
		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();
			$dat = array(
				'id' => get_the_ID(),
				'title' => get_the_title(),
				'content' => nl2br(get_the_content()),
			);
			$res = getHtml_modal_post($dat);
			
			$endScript .= 'siteInfo.ajax.getRecruitPost_top'.$postId.' = "'. rhiro_serializeValiable($res['html']).'";';
			$endScript .= 'createDialog(rhiro_unserializeValiable(siteInfo.ajax.getRecruitPost_top'.$postId.'), "'.$res['id'].'");';
			$endScript .= 'openDialog("'.$res['id'].'");';
		}
	}
	
	
	die($endScript);
}
add_action( 'wp_ajax_rhiro_ajax_displayModal_post', 'rhiro_ajax_displayModal_post' );
add_action( 'wp_ajax_nopriv_rhiro_ajax_displayModal_post', 'rhiro_ajax_displayModal_post' );


function getHtml_modal_post($dat='') {
	if(empty($dat) || !is_array($dat)) { return ''; }
	
	$res = array();
	$id = $dat['id'];
	$title = $dat['title'];
	$content = $dat['content'];
	$el_id = "modal-post-".$id;
	
	$res['id'] = $el_id;
	$res['html'] = <<<EOL
	<div id="$el_id" class="modal fade" role="dialog" aria-labelledby="$el_id" aria-hidden="true" style="display:none;"><div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<p class="modal-title">$title</p>
			</div>
			<div class="modal-body">$content</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-primary" data-dismiss="modal">閉じる</button>
			</div>
		</div>
	</div></div>
EOL;
	
	return $res;
}



