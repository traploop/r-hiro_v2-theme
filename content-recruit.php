<?php
/*
	リクルートの一覧を生成する
*/	
global $siteInfo;	


query_posts( array(
	'category_name' => 'recruit',
	'posts_per_page' => 10,
	'order' => 'DESC',
	'orderby' => 'date',
));


?>
<ul class="recruit_list  row">
<?php if( have_posts() ) : 
	while( have_posts() ) : the_post(); ?>
	<li class="col-xs-28 col-sm-20 col-sm-offset-4 row">
		<p class="title col-xs-28 col-sm-5"><?php the_title(); ?></p>
		<div class="content col-xs-28 col-sm-22 col-sm-offset-1"><?php the_content(); ?></div>
	</li>
<?php
	endwhile;
endif;

wp_reset_query();
?>
</ul>