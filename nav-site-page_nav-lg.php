<?php

// global_page_nav
if ( has_nav_menu( 'globalMenu' ) ) : ?>
	<nav class="nav-global_menu site-nav col-sm-28 row" role="navigation">
		<?php
			// Global navigation menu.
			wp_nav_menu( array(
				'menu_class'     => 'nav-menu1',
				'theme_location' => 'globalMenu',
				'link_before'    => '<span class="text">',
				'link_after'     => '</span>',
			) );
		?>
	</nav><!-- .main-navigation -->

<?php endif;

