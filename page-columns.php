<?php
/*
 Page : Columns
 
*/

global $siteInfo;

if(file_exists(get_template_directory().'/css/page-'.$name.'.css')) {
	wp_enqueue_style( 'page-'.$name, get_template_directory_uri() . '/css/page-'.$name.'.css' );
}



get_header(); 
?>

<article <?php post_class('page-'.$name); ?>>
<?php
query_posts( array(
	'category_name' => $name,
	'posts_per_page' => 10,
	'order' => 'DESC',
	'orderby' => 'date',
));

if( have_posts() ) : ?>
	<div class="site_contents  row inner-wrapper">
		<header class="row">
			<h2 class="col-xs-28 col-sm-7"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/'; ?>txt-column_index-top.svg" class="content-titile"></h2>
			<hr class="col-xs-28 col-sm-offset-1 col-sm-20">
			<p class="link_area  col-xs-28"><a href="/"><span class="text"><img src="<?php echo get_template_directory_uri(); ?>/images/shops/txt-group_home.svg" alt="GROUP HOME" /></span></a></p>
		</header>
		
		<div class="contents_area  row">
			<div class="index_area  col-xs-28 col-sm-18 col-sm-offset-1">
				<!-- START : WP contents -->
				<div class="column_list"><ul id="scroll-info_area">
	<?php
	while( have_posts() ) : the_post(); ?>
					<li>
						<a href="<?php the_permalink(); ?>"><p class="title_area"><span class="mark">●</span><span class="date"><?php echo get_the_date('Y.m.d'); ?></span><span class="title"><?php the_title(); ?></span></p></a>
					</li>
	<?php
	endwhile;
	?>
				</ul></div>
				<!-- END : WP contents -->
			</div>
			<div class="image_area  col-xs-28 col-sm-7 col-sm-offset-1">
				<figure><img class="img-responsive" src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>img-kazushige_yamaguchi.jpg"></figure>
				<p class="name"><span class="text"><img src="<?php echo get_template_directory_uri().'/images/'. $siteInfo['blogSlug'].'/' ?>txt-kazushige_yamaguchi.svg"></span></p>
			</div>
		</div>
	</div>
	<script>
		var viewScrollObj = {
			ob: jQuery('#scroll-info_area'),
			stopCount: 0,
			stopMax: 3,
			type: false,
			timer: false,
			'move': 120,
			'speed': 250,
			'interval': 600
		};
		
		function viewScroll(type) {
			if(type === undefined) { return false; }
			if(viewScrollObj.timer === false) {
				viewScrollObj.type = type;
				var mvPos = jQuery(viewScrollObj.ob).scrollTop();
				if(viewScrollObj.type === 'up') {
					mvPos -= viewScrollObj.move;
				} else {
					mvPos += viewScrollObj.move;
				}
				jQuery(viewScrollObj.ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing');
				
				viewScrollObj.timer = setInterval(function() {
					var ob = viewScrollObj.ob;
					var start = jQuery(ob).scrollTop();
					if(viewScrollObj.type === 'up') {
						var mvPos = (start - viewScrollObj.move);
						var nowPos = jQuery(ob).scrollTop();
						jQuery(ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing', function() {
							if(nowPos == jQuery(this).scrollTop()) {
								if(viewScrollObj.stopCount < viewScrollObj.stopMax) {
									viewScrollObj.stopCount++;
								} else {
									clearViewScroll();
									viewScrollObj.stopCount = 0;
								}
							}
						});
					} else {
						var mvPos = (start + viewScrollObj.move);
						var nowPos = jQuery(ob).scrollTop();
						jQuery(ob).animate({'scrollTop': mvPos + 'px'}, viewScrollObj.speed, 'swing', function() {
							if(nowPos == jQuery(this).scrollTop()) {
								if(viewScrollObj.stopCount < viewScrollObj.stopMax) {
									viewScrollObj.stopCount++;
								} else {
									clearViewScroll();
									viewScrollObj.stopCount = 0;
								}
							}
						});
					}
				}, viewScrollObj.interval);
			}
		}
		function clearViewScroll() {
			if(viewScrollObj.timer !== false) {
				clearInterval(viewScrollObj.timer);
				viewScrollObj.timer = false;
			}
		}
		jQuery("#view-scroll-up").hover(function() {
			viewScroll('up');
		}, function() {
			clearViewScroll();
		});
		jQuery("#view-scroll-down").hover(function() {
			viewScroll('down');
		}, function() {
			clearViewScroll();
		});
	</script>
<?php
endif;
wp_reset_query();
?>
</article>
<?php get_footer();
